#include "RegDialog.h"

RegDialog::RegDialog(QWidget *parent)
    :QDialog(parent,
             Qt::CustomizeWindowHint |
             Qt::WindowTitleHint |
             Qt::WindowCloseButtonHint
){
    setModal(true);
    setWindowTitle("Регистрация");

    m_pedtLogin = new QLineEdit;
    m_pedtPassword = new QLineEdit;
    m_pedtReplyPassword = new QLineEdit;
    m_pedtLName = new QLineEdit;
    m_pedtFName = new QLineEdit;
    m_pedtSName = new QLineEdit;
    m_pedtEmail = new QLineEdit;

    m_pedtPassword->setEchoMode(QLineEdit::Password);
    m_pedtReplyPassword->setEchoMode(QLineEdit::Password);

    m_pedtLogin->setMaxLength(20);
    m_pedtLName->setMaxLength(30);
    m_pedtFName->setMaxLength(30);
    m_pedtSName->setMaxLength(30);
    m_pedtEmail->setMaxLength(30);

    m_pedtLName->setValidator(&m_nameValidator);
    m_pedtFName->setValidator(&m_nameValidator);
    m_pedtSName->setValidator(&m_nameValidator);
    m_pedtEmail->setValidator(&m_emailValidator);

    m_plblLogin = new QLabel("Логин *");
    m_plblPassword = new QLabel("Пароль *");
    m_plblReplyPassword = new QLabel("Повторите пароль *");
    m_plblLName = new QLabel("Фамилия *");
    m_plblFName = new QLabel("Имя *");
    m_plblSName = new QLabel("Отчество");
    m_plblEmail = new QLabel("Email");

    m_pbtnReg = new QPushButton("&Регистрация");
    m_pbtnCancel = new QPushButton("&Отмена");
    m_pbtnReg->setDefault(true);

    // Соединение сигналов кнопок со слотами диалога
    connect(m_pbtnReg, SIGNAL(clicked()), SLOT(accept()));
    connect(m_pbtnCancel, SIGNAL(clicked()), SLOT(reject()));

    // Настройка компоновки
    m_pfrmLayout = new QFormLayout;
    m_pfrmLayout->addRow(m_plblLogin, m_pedtLogin);
    m_pfrmLayout->addRow(m_plblPassword, m_pedtPassword);
    m_pfrmLayout->addRow(m_plblReplyPassword, m_pedtReplyPassword);
    m_pfrmLayout->addRow(m_plblLName, m_pedtLName);
    m_pfrmLayout->addRow(m_plblFName, m_pedtFName);
    m_pfrmLayout->addRow(m_plblSName, m_pedtSName);
    m_pfrmLayout->addRow(m_plblEmail, m_pedtEmail);

    m_pbtnLayout = new QHBoxLayout;
    m_pbtnLayout->addStretch();
    m_pbtnLayout->addWidget(m_pbtnReg);
    m_pbtnLayout->addWidget(m_pbtnCancel);
    m_pbtnLayout->addStretch();

    m_playout = new QVBoxLayout;
    m_playout->addLayout(m_pfrmLayout);
    m_playout->addLayout(m_pbtnLayout);

    setLayout(m_playout);
}

void RegDialog::clear() {
// Возвращает форму в начальное состояние
    m_pedtLogin->setText("");
    m_pedtPassword->setText("");
    m_pedtReplyPassword->setText("");
    m_pedtLName->setText("");
    m_pedtFName->setText("");
    m_pedtSName->setText("");
    m_pedtEmail->setText("");

    m_pedtLogin->setFocus();
}

int RegDialog::exec() {
// Исполнение диалога
    clear();

    return QDialog::exec();
}

void RegDialog::accept() {
// Принять диалог
    if (!formIsValid()) {
        QMessageBox::critical(this,
                              "Ошибка ввода",
                              "Проверьте правильность введенных данных",
                              QMessageBox::Ok
                              );
        return;
    }
    if (m_pedtPassword->text() != m_pedtReplyPassword->text()) {
        QMessageBox::critical(this,
                              "Ошибка регистрации",
                              "Пароли не совпадают",
                              QMessageBox::Ok
                              );
        return;
    }

    QSqlQuery query;
    query.prepare("SELECT id FROM Users WHERE login = :login;");
    query.bindValue(":login", m_pedtLogin->text());
    query.exec();
    if (query.size() != 0) {
        QMessageBox::critical(this,
                              "Ошибка регистрации",
                              "Пользователь с таким логином уже существует",
                              QMessageBox::Ok
                              );
        return;
    }
    query.clear();

    QByteArray arr = m_pedtPassword->text().toUtf8();
    arr = QCryptographicHash::hash(arr, QCryptographicHash::Sha256);
    QString password = arr.toHex();

    query.prepare("INSERT INTO Users "
                  "(last_name, first_name, second_name, "
                  "login, password, email) "
                  "VALUES "
                  "(:last_name, :first_name, :second_name, "
                  ":login, :password, :email);"
                  );
    query.bindValue(":last_name", m_pedtLName->text());
    query.bindValue(":first_name", m_pedtFName->text());
    query.bindValue(":second_name", m_pedtSName->text());
    query.bindValue(":login", m_pedtLogin->text());
    query.bindValue(":password", password);
    query.bindValue(":email", m_pedtEmail->text());

    if (!query.exec()) {
        QMessageBox::critical(this,
                              "Ошибка регистрации",
                              "Не удалось добавить пользователя",
                              QMessageBox::Ok
                              );
        return;
    }

    QDialog::accept();
}

bool RegDialog::formIsValid() {
// Возвращает true, если все поля формы валидны
// Возвращает false иначе
    QString input; int pos = 0;

    if (m_pedtLogin->text() == "") return false;
    if (m_pedtPassword->text() == "") return false;
    if (m_pedtReplyPassword->text() == "") return false;
    input = m_pedtLName->text();
    if (m_nameValidator.validate(input, pos) != QValidator::Acceptable) return false;
    input = m_pedtFName->text();
    if (m_nameValidator.validate(input, pos) != QValidator::Acceptable) return false;
    input = m_pedtSName->text();
    if (m_nameValidator.validate(input, pos) != QValidator::Acceptable
            && !input.isEmpty()) return false;
    input = m_pedtEmail->text();
    if (m_emailValidator.validate(input, pos) != QValidator::Acceptable
            && !input.isEmpty()) return false;

    return true;
}
