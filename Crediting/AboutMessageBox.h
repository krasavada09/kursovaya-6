#ifndef ABOUTMESSAGEBOX_H
#define ABOUTMESSAGEBOX_H

#include <QPixmap>
#include <QMessageBox>

// Класс диалога "О программе"

class AboutMessageBox : public QMessageBox {
    Q_OBJECT
public:
    AboutMessageBox(QPixmap pixmap,
                    const QString &title,
                    const QString &text,
                    QMessageBox::StandardButtons buttons = NoButton,
                    QWidget *parent = nullptr,
                    Qt::WindowFlags f = Qt::Dialog | Qt::MSWindowsFixedSizeDialogHint
            );
};

#endif // ABOUTMESSAGEBOX_H
