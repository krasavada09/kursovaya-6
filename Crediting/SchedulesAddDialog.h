#ifndef SCHEDULESADDDIALOG_H
#define SCHEDULESADDDIALOG_H

#include "DbDialog.h"
#include "CreditCalc.h"

// Класс диалога добавления графика платежей
// Предназначен для внесения данных в БД

class SchedulesAddDialog : public DbDialog {
    Q_OBJECT
private:
    QLineEdit *m_pedtContractID;
    QLabel *m_plblContractID;
    QIntValidator m_intValidator;

    QPushButton *m_pbtnAdd;
    QPushButton *m_pbtnCancel;

    QFormLayout *m_pfrmLayout;
    QHBoxLayout *m_pbtnLayout;
    QVBoxLayout *m_playout;

public:
    SchedulesAddDialog(QWidget *parent = nullptr);

    void clear();
    bool formIsValid();

public slots:
    virtual int exec();
    virtual void accept();
};

#endif // SCHEDULESADDDIALOG_H
