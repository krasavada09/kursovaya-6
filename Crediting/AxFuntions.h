#ifndef AXFUNTIONS_H
#define AXFUNTIONS_H

#include <QString>
#include <QColor>
#include <QAxBase>
#include <QAxObject>
#include <QSqlQueryModel>

// Функции для работы с ActiveX

void writeSheet(QAxObject *sheet, QSqlQueryModel *model, QString tableName);

#endif // AXFUNTIONS_H
