#include "GroupsAddDialog.h"

GroupsAddDialog::GroupsAddDialog(QWidget *parent)
    : GroupsBaseDialog(parent) {
    setWindowTitle("Добавление группы");

    m_pbtnAdd = new QPushButton("&Добавить");
    m_pbtnCancel = new QPushButton("&Отмена");
    m_pbtnAdd->setDefault(true);

    // Слот reject() по клику на кнопку "Отмена"
    connect(m_pbtnCancel, SIGNAL(clicked()),
            this, SLOT(reject())
            );
    // Слот accept() по клику на кнопку "Добавить"
    connect(m_pbtnAdd, SIGNAL(clicked()),
            this, SLOT(accept())
            );

    // Настройка компоновки
    m_pbtnLayout = new QHBoxLayout;
    m_pbtnLayout->addStretch();
    m_pbtnLayout->addWidget(m_pbtnAdd);
    m_pbtnLayout->addWidget(m_pbtnCancel);
    m_pbtnLayout->addStretch();

    m_playout = new QVBoxLayout;
    m_playout->addLayout(m_pfrmLayout);
    m_playout->addLayout(m_pbtnLayout);

    setLayout(m_playout);
}

void GroupsAddDialog::accept() {
// Принять диалог
    if (!formIsValid()) {
        QMessageBox::critical(this,
                              "Ошибка ввода",
                              "Проверьте правильность введенных данных",
                              QMessageBox::Ok
                              );
        return;
    }

    QSqlQuery query;
    query.prepare(
                "INSERT INTO Groups "
                "(name, r, w, x) "
                "VALUES "
                "(:name, :r, :w, :x);"
                );
    query.bindValue(":name", m_pedtName->text());
    query.bindValue(":r", m_pchkR->isChecked());
    query.bindValue(":w", m_pchkW->isChecked());
    query.bindValue(":x", m_pchkX->isChecked());

    // Запрос на добавление группы
    if (!query.exec()) {
        QMessageBox::critical(this,
                              "Ошибка добавления",
                              "Не удалось добавить группу",
                              QMessageBox::Ok
                              );
        return;
    }

    QDialog::accept();
}
