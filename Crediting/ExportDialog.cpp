#include "ExportDialog.h"

ExportDialog::ExportDialog(
        QWidget *parent,
        QVector<AbstractEntityWidget *> *entities,
        QVector<QString> *tableNames)
    : QDialog(parent,
              Qt::CustomizeWindowHint |
              Qt::WindowTitleHint |
              Qt::WindowCloseButtonHint |
              Qt::WindowMaximizeButtonHint),
      m_pentities(entities),
      m_ptableNames(tableNames) {
    setWindowTitle("Экспорт");

    m_pchecks = new QVector<QCheckBox *>;
    for (int i = 0; i < m_ptableNames->size(); i++) {
        m_pchecks->append(new QCheckBox(m_ptableNames->at(i)));
    }

    m_plbl = new QLabel("Выберите таблицы для экспорта");

    m_pbtnExport = new QPushButton("&Экспорт");
    m_pbtnCancel = new QPushButton("&Отмена");
    m_pbtnExport->setDefault(true);

    // Слот reject() по клику на кнопку "Отмена"
    connect(m_pbtnCancel, SIGNAL(clicked()),
            this, SLOT(reject())
            );
    // Слот accept() по клику на кнопку "Экспорт"
    connect(m_pbtnExport, SIGNAL(clicked()),
            this, SLOT(accept())
            );

    // Настройка компоновки
    m_pbtnLayout = new QHBoxLayout;
    m_pbtnLayout->addStretch();
    m_pbtnLayout->addWidget(m_pbtnExport);
    m_pbtnLayout->addWidget(m_pbtnCancel);
    m_pbtnLayout->addStretch();

    m_playout = new QVBoxLayout;
    m_playout->addWidget(m_plbl);
    for (int i = 0; i < m_pchecks->size(); i++) {
        m_playout->addWidget(m_pchecks->at(i));
    }
    m_playout->addLayout(m_pbtnLayout);

    setLayout(m_playout);

    resize(250, height());
}

void ExportDialog::clear() {
// Возвращает форму в начальное состояние
    for (int i = 0; i < m_pchecks->size(); i++) {
        m_pchecks->at(i)->setChecked(false);
    }
    m_pchecks->first()->setFocus();
}

int ExportDialog::exec() {
// Исполнение диалога
    clear();

    return QDialog::exec();
}

void ExportDialog::accept() {
// Принять диалог
    QList<QString> keys;
    for (int i = 0; i < m_pchecks->size(); i++) {
        if (m_pchecks->at(i)->isChecked())
            keys.append(m_ptableNames->at(i));
    }

    if (keys.isEmpty()) {
        QMessageBox::critical(this,
                              "Ошибка экспорта",
                              "Не были выбраны таблицы",
                              QMessageBox::Ok
                              );
        return;
    }

    // Диалог сохранения файла
    QFileDialog dialog;
    dialog.setAcceptMode(QFileDialog::AcceptSave);
    dialog.setFileMode(QFileDialog::AnyFile);
    dialog.setNameFilters(QList<QString>() << "Книга Excel (*.xlsx)"
                                           << "Книга Excel 97-2003 (*.xls)"
                         );

    if (dialog.exec() == QDialog::Rejected) {
        QDialog::reject();
        return;
    }

    // Диалог прогресса операции
    QProgressDialog *progress = new QProgressDialog(
                "Сохранение файла", "&Отмена", 0, keys.size()+1, this,
                Qt::CustomizeWindowHint |
                Qt::WindowTitleHint |
                Qt::WindowCloseButtonHint
                );
    int progressCount = 0;
    progress->setWindowTitle("Пожалуйста, подождите");
    progress->setAutoClose(true);
    progress->show();
    progress->setValue(0);
    qApp->processEvents();

    QAxObject *excel,
              *workbooks,
              *workbook,
              *sheets,
              *sheet;

    // Создание документа
    excel = new QAxObject("Excel.Application", 0);
    excel->dynamicCall("SetDisplayAlerts(bool)", false);
    excel->dynamicCall("SetVisible(bool)", false);
    workbooks = excel->querySubObject("Workbooks");
    workbooks->dynamicCall("Add()");
    workbook = workbooks->querySubObject("Item(int)", 1);
    sheets = workbook->querySubObject("Worksheets");

    progress->setValue(++progressCount);
    qApp->processEvents();

    // Заполнение листов
    foreach (QString key, keys) {
        sheet = sheets->querySubObject("Item(int)", 1);
        writeSheet(sheet, m_pentities->at(m_ptableNames->indexOf(key))->model(), key);

        sheets->dynamicCall("Add()");

        if (progress->wasCanceled()) {
            workbook->dynamicCall("Close()");
            excel->dynamicCall("Quit()");
            QDialog::reject();
            return;
        }
        progress->setValue(++progressCount);
        qApp->processEvents();
    }
    sheets->querySubObject("Item(int)", 1)->dynamicCall("Delete()");

    // Сохранение файла
    workbook->dynamicCall("SaveCopyAS(const QVariant &)",
                          QVariant(dialog.selectedFiles().first()));
    workbook->dynamicCall("Close()");
    excel->dynamicCall("Quit()");

    QDialog::accept();
}
