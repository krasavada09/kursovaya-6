#include "StyledSortFilterProxyModel.h"

#include <QDebug>

StyledSortFilterProxyModel::StyledSortFilterProxyModel(QObject *parent)
    : QSortFilterProxyModel(parent) {
    m_pidList = new QList<int>;
}

void StyledSortFilterProxyModel::setIDs(QList<int> idList) {
// Сеттер для m_pidList
// Принимает QList с id записей
    *m_pidList = idList;
}

void StyledSortFilterProxyModel::clearIDs() {
// Очищает QList с id записей
    m_pidList->clear();
}

QVariant StyledSortFilterProxyModel::data(const QModelIndex &index, int role) const {
    if (role == Qt::BackgroundRole) {
        // id записи по индексу
        int id = index.model()->index(index.row(), 0).data().toInt();
        QColor color = m_pidList->contains(id) ?
                    QColor(Qt::yellow) : QColor(Qt::white);
        return QBrush(color);
    }

    return QSortFilterProxyModel::data(index, role);
}
