#ifndef USERSADDDIALOG_H
#define USERSADDDIALOG_H

#include "UsersBaseDialog.h"

// Класс диалога добавления клиента
// Предназначен для внесения данных в БД

class UsersAddDialog : public UsersBaseDialog {
    Q_OBJECT
private:
    QPushButton *m_pbtnAdd;                             // Кнопки диалога
    QPushButton *m_pbtnCancel;

    QHBoxLayout *m_pbtnLayout;                          // Компоновки диалога
    QVBoxLayout *m_playout;

public:
    UsersAddDialog(QWidget *parent = nullptr);

public slots:
    virtual void accept();
};

#endif // USERSADDDIALOG_H
