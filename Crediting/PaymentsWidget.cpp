#include "PaymentsWidget.h"

PaymentsWidget::PaymentsWidget(QWidget *parent)
    : AbstractEntityWidget(parent) {
    // Настройка компоновки кнопок
    m_pbtnLayout = new QHBoxLayout;
    m_pbtnLayout->addWidget(m_pbtnFirst);
    m_pbtnLayout->addWidget(m_pbtnPrevious);
    m_pbtnLayout->addWidget(m_pbtnNext);
    m_pbtnLayout->addWidget(m_pbtnLast);
    m_pbtnLayout->addWidget(m_pbtnFind);
    m_pbtnLayout->addStretch();

    // Настройка компоновки виджета
    m_playout = new QVBoxLayout;
    m_playout->addLayout(m_pbtnLayout);
    m_playout->addWidget(m_ptblView);

    setLayout(m_playout);
}

void PaymentsWidget::setContextMenu() {
// Устанавливает контекстное меню
    m_pmenu->addAction("Поиск", [this]() {
        m_pfindDialog->exec(m_pproxyModel);
    });
    m_pmenu->addAction("Обновить", this, SLOT(refresh()));
}
