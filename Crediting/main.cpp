#include <QtWidgets>

#include "MainWindow.h"
#include "DatabaseConnection.h"
#include "AuthDialog.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setWindowIcon(QIcon("images/bank-svgrepo-com.svg"));

    AuthDialog *auth = new AuthDialog();

    if (!createConnection()) {
        QMessageBox::critical(auth,
                              "Ошибка подключения",
                              "Не удалось подключиться к базе данных",
                              QMessageBox::Ok
                              );
        return -1;
    }

    QApplication::connect(auth, &AuthDialog::login,
                          [](User *user) {
        MainWindow *mainWindow = new MainWindow(user);
        mainWindow->resize(1280, 720);
        mainWindow->show();
    }
    );

    auth->show();

    return a.exec();
}
