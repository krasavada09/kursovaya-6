#include "ProfitReportDialog.h"

ProfitReportDialog::ProfitReportDialog(QWidget *parent)
    : QDialog(parent,
              Qt::CustomizeWindowHint |
              Qt::WindowTitleHint |
              Qt::WindowCloseButtonHint |
              Qt::WindowMaximizeButtonHint
            ) {
    setWindowTitle("Сформировать отчет");

    m_pdteFirstDate = new QDateEdit;
    m_pdteLastDate = new QDateEdit;

    m_pdteFirstDate->setMaximumWidth(150);
    m_pdteLastDate->setMaximumWidth(150);

    m_pdteFirstDate->setCalendarPopup(true);
    m_pdteLastDate->setCalendarPopup(true);

    m_plblFirstDate = new QLabel("Дата c *:");
    m_plblLastDate = new QLabel("Дата по *:");

    m_pbtnReport = new QPushButton("&Формировать");
    m_pbtnCancel = new QPushButton("&Отмена");
    m_pbtnReport->setDefault(true);

    // Слот reject() по клику на кнопку "Отмена"
    connect(m_pbtnCancel, SIGNAL(clicked()),
            this, SLOT(reject())
            );
    // Слот accept() по клику на кнопку "Формировать"
    connect(m_pbtnReport, SIGNAL(clicked()),
            this, SLOT(accept())
            );

    // Настройка компоновки
    m_pfrmLayout = new QFormLayout;
    m_pfrmLayout->addRow(m_plblFirstDate, m_pdteFirstDate);
    m_pfrmLayout->addRow(m_plblLastDate, m_pdteLastDate);
    m_pfrmLayout->setSpacing(10);

    m_pbtnLayout = new QHBoxLayout;
    m_pbtnLayout->addStretch();
    m_pbtnLayout->addWidget(m_pbtnReport);
    m_pbtnLayout->addWidget(m_pbtnCancel);
    m_pbtnLayout->addStretch();

    m_playout = new QVBoxLayout;
    m_playout->addItem(m_pfrmLayout);
    m_playout->addItem(m_pbtnLayout);

    setLayout(m_playout);

    resize(300, height());
}

void ProfitReportDialog::clear() {
// Возвращает форму в начальное состояние
    m_pdteFirstDate->setDate(QDate::currentDate());
    m_pdteLastDate->setDate(QDate::currentDate());

    m_pdteFirstDate->setFocus();
}

int ProfitReportDialog::exec() {
// Исполнение диалога
    clear();

    return QDialog::exec();
}

void ProfitReportDialog::accept() {
// Принять диалог
    QSqlQueryModel model;
    QString queryString;
    queryString = "WITH Payment_Schedules_Current AS ( "
                  "SELECT "
                  "MAX(id) AS id, credit_contract_id "
                  "FROM Payment_Schedules "
                  "GROUP BY credit_contract_id "
                  ") SELECT "
                  "psc.id, psc.credit_contract_id AS \"№ договора\", "
                  "SUM(si.amount) AS \"Ожидаемая выплата\" "
                  "FROM Payment_Schedules_Current psc "
                  "JOIN Schedule_Items si ON (si.payment_schedule_id = psc.id) "
                  "WHERE si.date BETWEEN '%1' AND '%2' "
                  "GROUP BY psc.id, psc.credit_contract_id;";
    queryString = queryString.arg(m_pdteFirstDate->date().toString("yyyy-MM-dd"))
                             .arg(m_pdteLastDate->date().toString("yyyy-MM-dd"));

    model.setQuery(queryString);
    if (model.lastError().type() != QSqlError::NoError) {
        QMessageBox::critical(this,
                              "Ошибка чтения",
                              "Не удалось считать данные договоров",
                              QMessageBox::Ok
                              );
        return;
    }
    if (model.rowCount() < 1) {
        QMessageBox::information(this,
                              "Записи не найдены",
                              "Нет записей, удовлетворяющих условиям",
                              QMessageBox::Ok
                              );
        return;
    }

    QAxObject *excel,
              *workbooks,
              *workbook,
              *sheets,
              *sheet;

    // Создание документа
    excel = new QAxObject("Excel.Application", 0);
    excel->dynamicCall("SetDisplayAlerts(bool)", false);
    excel->dynamicCall("SetVisible(bool)", false);
    workbooks = excel->querySubObject("Workbooks");
    workbooks->dynamicCall("Add()");
    workbook = workbooks->querySubObject("Item(int)", 1);
    sheets = workbook->querySubObject("Worksheets");
    sheet = sheets->querySubObject("Item(int)", 1);

    // Заполнение листа
    writeSheet(sheet, &model, "Прибыль банка");

    excel->dynamicCall("SetDisplayAlerts(bool)", true);
    excel->dynamicCall("SetVisible(bool)", true);

    QDialog::accept();
}
