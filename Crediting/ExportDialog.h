#ifndef EXPORTDIALOG_H
#define EXPORTDIALOG_H

#include <QDialog>
#include <QAxBase>
#include <QAxObject>
#include "AbstractEntityWidget.h"
#include "AxFuntions.h"

// Класс диалога экспорта
// Реализует экспорт таблиц сущностей

class ExportDialog : public QDialog {
    Q_OBJECT
private:
    QVector<AbstractEntityWidget *> *m_pentities;            // Сущности БД
    QVector<QString> *m_ptableNames;                         // Имена таблиц БД
    QVector<QCheckBox *> *m_pchecks;
    QLabel *m_plbl;

    QPushButton *m_pbtnExport;                               // Кнопки диалога
    QPushButton *m_pbtnCancel;

    QHBoxLayout *m_pbtnLayout;                               // Компоновки диалога
    QVBoxLayout *m_playout;

public:
    ExportDialog(QWidget *parent,
                 QVector<AbstractEntityWidget *> *entities,
                 QVector<QString> *tableNames
                 );

    void clear();

public slots:
    virtual int exec();
    virtual void accept();
};

#endif // EXPORTDIALOG_H
