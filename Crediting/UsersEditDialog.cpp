#include "UsersEditDialog.h"

UsersEditDialog::UsersEditDialog(QWidget *parent)
    : UsersBaseDialog(parent) {
    setWindowTitle("Редактирование пользователя");

    m_pbtnSave = new QPushButton("&Сохранить");
    m_pbtnCancel = new QPushButton("&Отмена");
    m_pbtnSave->setDefault(true);

    // Слот reject() по клику на кнопку "Отмена"
    connect(m_pbtnCancel, SIGNAL(clicked()),
            this, SLOT(reject())
            );
    // Слот accept() по клику на кнопку "Сохранить"
    connect(m_pbtnSave, SIGNAL(clicked()),
            this, SLOT(accept())
            );

    // Настройка компоновки
    m_pbtnLayout = new QHBoxLayout;
    m_pbtnLayout->addStretch();
    m_pbtnLayout->addWidget(m_pbtnSave);
    m_pbtnLayout->addWidget(m_pbtnCancel);
    m_pbtnLayout->addStretch();

    m_playout = new QVBoxLayout;
    m_playout->addLayout(m_pfrmLayout);
    m_playout->addLayout(m_pbtnLayout);

    setLayout(m_playout);
}

void UsersEditDialog::accept() {
// Принять диалог
    if (!formIsValid()) {
        QMessageBox::critical(this,
                              "Ошибка ввода",
                              "Проверьте правильность введенных данных",
                              QMessageBox::Ok
                              );
        return;
    }

    QSqlQuery query;
    query.prepare(
                "UPDATE Users "
                "SET "
                "group_id = :group_id, last_name = :last_name, "
                "first_name = :first_name, second_name = :second_name, "
                "login = :login, email = :email "
                "WHERE id = :id;"
                );
    query.bindValue(":group_id",
                    m_pcbbGroupID->currentData().toString().isEmpty() ?
                        QVariant(QVariant::String) :
                    m_pcbbGroupID->currentData().toString());
    query.bindValue(":last_name", m_pedtLName->text());
    query.bindValue(":first_name", m_pedtFName->text());
    query.bindValue(":second_name", m_pedtSName->text());
    query.bindValue(":login", m_pedtLogin->text());
    query.bindValue(":email", m_pedtEmail->text());
    query.bindValue(":id", m_id);

    // Запрос на обновление клиента
    if (!query.exec()) {
        QMessageBox::critical(this,
                              "Ошибка редактирования",
                              "Не удалось сохранить пользователя",
                              QMessageBox::Ok
                              );
        return;
    }

    QDialog::accept();
}
