#ifndef CREDITCONTRACTSADDDIALOG_H
#define CREDITCONTRACTSADDDIALOG_H

#include "CreditContractsBaseDialog.h"

// Класс диалога добавления договора
// Предназначен для внесения данных в БД

class CreditContractsAddDialog : public CreditContractsBaseDialog {
    Q_OBJECT
private:
    QPushButton *m_pbtnAdd;                             // Кнопки диалога
    QPushButton *m_pbtnCancel;

    QHBoxLayout *m_pbtnLayout;                          // Компоновки диалога
    QVBoxLayout *m_playout;

public:
    CreditContractsAddDialog(int userID, QWidget *parent = nullptr);

public slots:
    virtual void accept();
};

#endif // CREDITCONTRACTSADDDIALOG_H
