#ifndef USERSBASEDIALOG_H
#define USERSBASEDIALOG_H

#include "DbDialog.h"
#include "NameValidator.h"
#include "EmailValidator.h"

// Класс базового диалога пользователя
// Определяет поля, методы открытия формы

class UsersBaseDialog : public DbDialog {
    Q_OBJECT
protected:
    int m_id;                                      // ID записи

    QComboBox *m_pcbbGroupID;                      // Поля диалога
    QLineEdit *m_pedtLName;
    QLineEdit *m_pedtFName;
    QLineEdit *m_pedtSName;
    QLineEdit *m_pedtLogin;
    QLineEdit *m_pedtEmail;

    NameValidator m_nameValidator;                 // Валидаторы данных
    EmailValidator m_emailValidator;

    QLabel *m_plblGroupID;                         // Подписи диалога
    QLabel *m_plblLName;
    QLabel *m_plblFName;
    QLabel *m_plblSName;
    QLabel *m_plblLogin;
    QLabel *m_plblEmail;

    QFormLayout *m_pfrmLayout;                     // Компоновка полей

public:
    UsersBaseDialog(QWidget *parent = nullptr);

    void clear();
    bool formIsValid();

public slots:
    virtual int exec();
    virtual int exec(int &id);
};

#endif // USERSBASEDIALOG_H
