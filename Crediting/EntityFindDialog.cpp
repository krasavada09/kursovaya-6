#include "EntityFindDialog.h"

EntityFindDialog::EntityFindDialog(QWidget *parent)
    : DbDialog(parent,
               Qt::CustomizeWindowHint |
               Qt::WindowTitleHint |
               Qt::WindowCloseButtonHint |
               Qt::WindowMaximizeButtonHint) {
    setWindowTitle("Поиск");

    m_pcbbField = new QComboBox;
    m_pedtValue = new QLineEdit;
    m_pradFilter = new QRadioButton("Выборка");
    m_pradMark = new QRadioButton("Подсветка");
    m_pchkCaseSensivity = new QCheckBox("Чувствительность к регистру");

    m_plblField = new QLabel("Поле поиска");
    m_plblValue = new QLabel("Значение");

    m_pbtnFind = new QPushButton("&Поиск");
    m_pbtnCancel = new QPushButton("&Отмена");
    m_pbtnFind->setDefault(true);

    // Клик на кнопку "Отмена"
    connect(m_pbtnCancel, &QPushButton::clicked,
            [this]() {cancel();}
            );
    // Слот reject() по клику на кнопку "Отмена"
    connect(m_pbtnCancel, SIGNAL(clicked()),
            this, SLOT(reject())
            );
    // Слот accept() по клику на кнопку "Поиск"
    connect(m_pbtnFind, SIGNAL(clicked()),
            this, SLOT(accept())
            );

    // Настройка компоновки
    m_pfrmLayout = new QFormLayout;
    m_pfrmLayout->addRow(m_plblField, m_pcbbField);
    m_pfrmLayout->addRow(m_plblValue, m_pedtValue);
    m_pfrmLayout->setHorizontalSpacing(10);

    m_pbtnLayout = new QHBoxLayout;
    m_pbtnLayout->addStretch();
    m_pbtnLayout->addWidget(m_pbtnFind);
    m_pbtnLayout->addWidget(m_pbtnCancel);
    m_pbtnLayout->addStretch();

    m_playout = new QVBoxLayout;
    m_playout->addItem(m_pfrmLayout);
    m_playout->addWidget(m_pchkCaseSensivity);
    m_playout->addWidget(m_pradFilter);
    m_playout->addWidget(m_pradMark);
    m_playout->addItem(m_pbtnLayout);

    setLayout(m_playout);
}

int EntityFindDialog::exec() {
    return QDialog::exec();
}

int EntityFindDialog::exec(int &) {
    return QDialog::exec();
}

int EntityFindDialog::exec(StyledSortFilterProxyModel *model) {
// Исполнение диалога
    m_pentityModel = model;
    clear();

    return QDialog::exec();
}

void EntityFindDialog::clear() {
// Возвращает форму в начальное состояние
    m_pcbbField->clear();
    m_pedtValue->setText("");
    m_pradFilter->setChecked(true);
    m_pchkCaseSensivity->setChecked(true);

    m_pcbbField->addItem("Все поля", -1);
    // Перебор полей модели (без поля id)
    for (int i = 1; i < m_pentityModel->columnCount(); i++) {
        m_pcbbField->addItem(
                    m_pentityModel->headerData(i, Qt::Horizontal).toString(),
                    i
                    );
    }
}

void EntityFindDialog::accept() {
// Принять диалог
    cancel();

    if (m_pradFilter->isChecked()) filter();
    if (m_pradMark->isChecked()) mark();
}

void EntityFindDialog::filter(){
// Фильтрует найденные записи таблицы
    m_pentityModel->setFilterKeyColumn(m_pcbbField->currentData().toInt());
    m_pentityModel->setFilterWildcard(m_pedtValue->text());

    if (m_pchkCaseSensivity->isChecked())
        m_pentityModel->setFilterCaseSensitivity(Qt::CaseSensitive);
    else
        m_pentityModel->setFilterCaseSensitivity(Qt::CaseInsensitive);
}

void EntityFindDialog::mark() {
// Выделяет найденные записи таблицы
    // Фильтрация
    filter();

    // Заполнение списка id
    QList<int> idList;
    for (int i = 0; i < m_pentityModel->rowCount(); i++) {
        QModelIndex index = m_pentityModel->index(i, 0);
        idList << m_pentityModel->data(index).toInt();
    }
    m_pentityModel->setIDs(idList);

    // Отмена фильтрации
    m_pentityModel->setFilterKeyColumn(-1);
    m_pentityModel->setFilterWildcard("");
    // Перерисовка представления
    emit m_pentityModel->layoutChanged();
}

void EntityFindDialog::cancel() {
// Отменяет поиск записей
    m_pentityModel->setFilterKeyColumn(-1);
    m_pentityModel->setFilterWildcard("");

    m_pentityModel->clearIDs();
    emit m_pentityModel->layoutChanged();
}
