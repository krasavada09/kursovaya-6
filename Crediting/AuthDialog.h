#ifndef AUTHDIALOG_H
#define AUTHDIALOG_H

#include <QtWidgets>
#include <QtSql>
#include "User.h"
#include "RegDialog.h"

// Класс диалога авторизации
// Реализует авторизацию пользователя

class AuthDialog : public QDialog {
    Q_OBJECT;
private:
    QLineEdit *m_pedtLogin;                     // Поля диалога
    QLineEdit *m_pedtPassword;

    QLabel *m_plblLogin;                        // Подписи диалога
    QLabel *m_plblPassword;

    QPushButton *m_pbtnEnter;                   // Кнопки диалога
    QPushButton *m_pbtnCancel;
    QPushButton *m_pbtnReg;

    RegDialog *m_pregDialog;

    QFormLayout *m_pfrmLayout;                  // Компоновки диалога
    QHBoxLayout *m_pbtnLayout;
    QVBoxLayout *m_playout;

public:
    AuthDialog(QWidget *parent = nullptr);

    void clear();
    bool formIsValid();

public slots:
    virtual int exec();
    virtual void accept();

signals:
    void login(User *user);
    void logout();

};

#endif // AUTHDIALOG_H
