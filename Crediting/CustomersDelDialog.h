#ifndef CUSTOMERSDELDIALOG_H
#define CUSTOMERSDELDIALOG_H

#include "DbDialog.h"

// Класс диалога удаления клиента
// Предназначен для удаления данных из БД

class CustomersDelDialog : public DbDialog {
    Q_OBJECT
private:
    int m_id;                                // ID Клиента

    QLabel *m_plbl;                          // Подпись диалога
    QPushButton *m_pbtnYes;                  // Кнопки диалога
    QPushButton *m_pbtnNo;

    QHBoxLayout *m_pbtnLayout;               // Компоновки диалога
    QVBoxLayout *m_playout;

public:
    CustomersDelDialog(QWidget *parent = nullptr);

public slots:
    virtual int exec(int &id);
    virtual void accept();
};

#endif // CUSTOMERSDELDIALOG_H
