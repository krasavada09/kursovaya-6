#include "CreditContractsEditDialog.h"

CreditContractsEditDialog::CreditContractsEditDialog(QWidget *parent)
    :CreditContractsBaseDialog(parent) {
    setWindowTitle("Редактирование договора");

    m_pbtnSave = new QPushButton("&Сохранить");
    m_pbtnCancel = new QPushButton("&Отмена");
    m_pbtnSave->setDefault(true);

    // Слот reject() по клику на кнопку "Отмена"
    connect(m_pbtnCancel, SIGNAL(clicked()),
            this, SLOT(reject())
            );
    // Слот accept() по клику на кнопку "Сохранить"
    connect(m_pbtnSave, SIGNAL(clicked()),
            this, SLOT(accept())
            );

    // Настройка компоновки
    m_pbtnLayout = new QHBoxLayout;
    m_pbtnLayout->addStretch();
    m_pbtnLayout->addWidget(m_pbtnSave);
    m_pbtnLayout->addWidget(m_pbtnCancel);
    m_pbtnLayout->addStretch();

    m_playout = new QVBoxLayout;
    m_playout->addLayout(m_pfrmLayout);
    m_playout->addLayout(m_pbtnLayout);

    setLayout(m_playout);
}

void CreditContractsEditDialog::accept() {
// Принять диалог
    if (!formIsValid()) {
        QMessageBox::critical(this,
                              "Ошибка ввода",
                              "Проверьте правильность введенных данных",
                              QMessageBox::Ok
                              );
        return;
    }

    QSqlQuery query;
    query.prepare(
                "UPDATE Credit_Contracts "
                "SET "
                "provision_id = :provision_id, "
                "purpose_id = :purpose_id, repayment_method_id = :repayment_method_id, "
                "form_id = :form_id, customer_id = :customer_id, amount = :amount, "
                "rate = :rate, term = :term, date = :date, "
                "insurance_contract_id = :insurance_contract_id "
                "WHERE id = :id;"
                );
    query.bindValue(":provision_id", m_pcbbProvisionID->currentData().toString());
    query.bindValue(":purpose_id", m_pcbbPurposeID->currentData().toString());
    query.bindValue(":repayment_method_id",
                    m_pcbbRepaymentMethodID->currentData().toString()
                    );
    query.bindValue(":form_id", m_pcbbFormID->currentData().toString());
    query.bindValue(":customer_id", m_pcbbCustomerID->currentData().toString());
    QLocale locale(QLocale::Russian);
    query.bindValue(":amount", locale.toDouble(m_pedtAmount->text()));
    query.bindValue(":rate", locale.toDouble(m_pedtRate->text()));
    query.bindValue(":term", m_pedtTerm->text().toInt());
    query.bindValue(":date", m_pdteDate->date().toString("yyyy-MM-dd"));
    query.bindValue(":insurance_contract_id",
                    m_pedtInsuranceContractID->text().isEmpty() ?
                        QVariant(QVariant::String) :
                        m_pedtInsuranceContractID->text()
                    );
    query.bindValue(":id", m_id);

    // Запрос на обновление клиента
    if (!query.exec()) {
        QMessageBox::critical(this,
                              "Ошибка редактирования",
                              "Не удалось сохранить договор",
                              QMessageBox::Ok
                              );
        return;
    }

    QDialog::accept();
}
