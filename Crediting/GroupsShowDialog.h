#ifndef GROUPSSHOWDIALOG_H
#define GROUPSSHOWDIALOG_H

#include "GroupsBaseDialog.h"

// Класс диалога просмотра группы
// Предназначен для вывода информации из БД

class GroupsShowDialog : public GroupsBaseDialog {
    Q_OBJECT
private:
    QPushButton *m_pbtnOk;                      // Кнопка диалога

    QHBoxLayout *m_pbtnLayout;                  // Компоновки диалога
    QVBoxLayout *m_playout;

public:
    GroupsShowDialog(QWidget *parent = nullptr);
};

#endif // GROUPSSHOWDIALOG_H
