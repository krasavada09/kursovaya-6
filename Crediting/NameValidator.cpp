#include "NameValidator.h"

NameValidator::NameValidator(QObject *parent)
    : QRegExpValidator(parent) {
    setRegExp(QRegExp("^\\D+$"));
}
