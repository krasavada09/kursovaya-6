#include "UsersShowDialog.h"

UsersShowDialog::UsersShowDialog(QWidget *parent)
    : UsersBaseDialog(parent) {
    setWindowTitle("Просмотр пользователя");

    m_pbtnOk = new QPushButton("&Ок");
    m_pbtnOk->setDefault(true);

    // Слот accept() по клику на кнопку "Ок"
    connect(m_pbtnOk, SIGNAL(clicked()),
            this, SLOT(accept())
            );

    // Настройка компоновки
    m_pbtnLayout = new QHBoxLayout;
    m_pbtnLayout->addStretch();
    m_pbtnLayout->addWidget(m_pbtnOk);
    m_pbtnLayout->addStretch();

    m_playout = new QVBoxLayout;
    m_playout->addLayout(m_pfrmLayout);
    m_playout->addLayout(m_pbtnLayout);

    setLayout(m_playout);
}
