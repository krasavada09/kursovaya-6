#include "PaymentSchedulesWidget.h"

PaymentSchedulesWidget::PaymentSchedulesWidget(QWidget *parent)
    : AbstractEntityWidget(parent) {
    m_pbtnAdd->setText("Сформировать график...");

    // Настройка компоновки кнопок
    m_pbtnLayout = new QHBoxLayout;
    m_pbtnLayout->addWidget(m_pbtnFirst);
    m_pbtnLayout->addWidget(m_pbtnPrevious);
    m_pbtnLayout->addWidget(m_pbtnNext);
    m_pbtnLayout->addWidget(m_pbtnLast);
    m_pbtnLayout->addWidget(m_pbtnAdd);
    m_pbtnLayout->addWidget(m_pbtnFind);
    m_pbtnLayout->addStretch();

    // Настройка компоновки виджета
    m_playout = new QVBoxLayout;
    m_playout->addLayout(m_pbtnLayout);
    m_playout->addWidget(m_ptblView);

    setLayout(m_playout);
}

void PaymentSchedulesWidget::setContextMenu() {
// Устанавливает контекстное меню
    m_pmenu->addAction("Сформировать...", m_paddDialog, SLOT(exec()));
    m_pmenu->addSeparator();
    m_pmenu->addAction("Поиск", [this]() {
        m_pfindDialog->exec(m_pproxyModel);
    });
    m_pmenu->addAction("Обновить", this, SLOT(refresh()));
}
