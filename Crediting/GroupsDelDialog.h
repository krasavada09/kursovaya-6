#ifndef GROUPSDELDIALOG_H
#define GROUPSDELDIALOG_H

#include "DbDialog.h"

// Класс диалога удаления группы
// Предназначен для удаления данных из БД

class GroupsDelDialog: public DbDialog {
    Q_OBJECT
private:
    int m_id;                                // ID группы

    QLabel *m_plbl;                          // Подпись диалога
    QPushButton *m_pbtnYes;                  // Кнопки диалога
    QPushButton *m_pbtnNo;

    QHBoxLayout *m_pbtnLayout;               // Компоновки диалога
    QVBoxLayout *m_playout;

public:
    GroupsDelDialog(QWidget *parent = nullptr);

public slots:
    virtual int exec(int &id);
    virtual void accept();
};

#endif // GROUPSDELDIALOG_H
