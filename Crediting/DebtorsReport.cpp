#include "DebtorsReport.h"

DebtorsReport::DebtorsReport(QWidget *parent)
    : QWidget(parent) {}

int DebtorsReport::exec() {
// Формирование отчета
    QSqlQueryModel model;
    model.setQuery("WITH Schedules AS ( "
                   "SELECT "
                   "cc.id AS \"credit_contract_id\", "
                   "cc.customer_id, "
                   "cc.date, cc.term, "
                   "max(ps.id) AS \"schedule_id\", "
                   "cc.amount::numeric "
                   "FROM Credit_Contracts cc "
                   "JOIN Payment_Schedules ps ON (ps.credit_contract_id = cc.id) "
                   "GROUP BY cc.id "
                   ") "
                   "SELECT "
                   "s.customer_id, "
                   "s.credit_contract_id AS \"№ договора\", "
                   "c.last_name || ' ' || "
                   "c.first_name || ' ' || "
                   "c.second_name AS \"ФИО должника\", "
                   "s.date AS \"Дата выдачи\", s.term AS \"Срок\", "
                   "sum(si.amount::numeric) AS \"Сумма\" "
                   "FROM Schedules s "
                   "JOIN Schedule_Items si ON (s.schedule_id = si.payment_schedule_id) "
                   "JOIN Customers c ON (c.id = s.customer_id) "
                   "GROUP BY "
                   "s.credit_contract_id, "
                   "s.customer_id, "
                   "c.last_name || ' ' || c.first_name || ' ' || c.second_name, "
                   "s.term, s.date "
                   "ORDER BY \"ФИО должника\", \"Дата выдачи\", credit_contract_id;"
                   );
    if (model.lastError().type() != QSqlError::NoError) {
        QMessageBox::critical(this,
                              "Ошибка чтения",
                              "Не удалось считать данные клиентов",
                              QMessageBox::Ok
                              );
        qDebug() << model.lastError();
        return 0;
    }
    if (model.rowCount() < 1) {
        QMessageBox::information(this,
                              "Записи не найдены",
                              "Нет записей, удовлетворяющих условиям",
                              QMessageBox::Ok
                              );
        return 0;
    }

    QAxObject *excel,
              *workbooks,
              *workbook,
              *sheets,
              *sheet;

    // Создание документа
    excel = new QAxObject("Excel.Application", 0);
    excel->dynamicCall("SetDisplayAlerts(bool)", false);
    excel->dynamicCall("SetVisible(bool)", false);
    workbooks = excel->querySubObject("Workbooks");
    workbooks->dynamicCall("Add()");
    workbook = workbooks->querySubObject("Item(int)", 1);
    sheets = workbook->querySubObject("Worksheets");
    sheet = sheets->querySubObject("Item(int)", 1);

    // Заполнение листа
    writeSheet(sheet, &model, "Должники по кредитам");

    excel->dynamicCall("SetDisplayAlerts(bool)", true);
    excel->dynamicCall("SetVisible(bool)", true);

    return 1;
}
