#ifndef DEBTORSREPORT_H
#define DEBTORSREPORT_H

#include <QtWidgets>
#include <QtSql>
#include <QAxBase>
#include <QAxObject>
#include "AxFuntions.h"

// Отчет "Должники"

class DebtorsReport : public QWidget {
    Q_OBJECT
public:
    DebtorsReport(QWidget *parent = nullptr);

public slots:
    int exec();
};

#endif // DEBTORSREPORT_H
