#include "DatabaseConnection.h"

bool createConnection() {
// Создание подключения к базе данных
    QSqlDatabase db = QSqlDatabase::addDatabase("QPSQL");
    db.setDatabaseName("crediting");
    db.setHostName("localhost");
    db.setPort(5432);
    db.setUserName("postgres");
    db.setPassword("example");

    return (db.open()) ? true : false;
}
