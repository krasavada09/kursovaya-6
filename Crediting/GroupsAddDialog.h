#ifndef GROUPSADDDIALOG_H
#define GROUPSADDDIALOG_H

#include "GroupsBaseDialog.h"

// Класс диалога добавления группы
// Предназначен для внесения данных в БД

class GroupsAddDialog : public GroupsBaseDialog {
    Q_OBJECT
private:
    QPushButton *m_pbtnAdd;                             // Кнопки диалога
    QPushButton *m_pbtnCancel;

    QHBoxLayout *m_pbtnLayout;                          // Компоновки диалога
    QVBoxLayout *m_playout;

public:
    GroupsAddDialog(QWidget *parent = nullptr);

public slots:
    virtual void accept();
};

#endif // GROUPSADDDIALOG_H
