#ifndef PAYMENTSCHEDULEREPORTDIALOG_H
#define PAYMENTSCHEDULEREPORTDIALOG_H

#include <QtWidgets>
#include <QtSql>
#include <QAxBase>
#include <QAxObject>
#include "AxFuntions.h"

// Диалог отчета "График платежей"
// Предназначен для формирования отчета

class PaymentScheduleReportDialog: public QDialog {
    Q_OBJECT
private:
    QLineEdit *m_pedtContractID;                    // Поле диалога
    QIntValidator m_intValidator;                   // Валидатор данных
    QLabel *m_plblContractID;                       // Подпись диалога

    QPushButton *m_pbtnReport;                      // Кнопки диалога
    QPushButton *m_pbtnCancel;

    QFormLayout *m_pfrmLayout;                      // Компоновки диалога
    QHBoxLayout *m_pbtnLayout;
    QVBoxLayout *m_playout;

public:
    PaymentScheduleReportDialog(QWidget *parent = nullptr);

    void clear();
    bool formIsValid();

public slots:
    virtual int exec();
    virtual void accept();
};

#endif // PAYMENTSCHEDULEREPORTDIALOG_H
