#ifndef DBDIALOG_H
#define DBDIALOG_H

#include <QtWidgets>
#include <QtSql>

// Класс диалога с данными о модели сущности
// Необходим для определения метода exec()
// с параметром int &

class DbDialog : public QDialog {
    Q_OBJECT
public:
    DbDialog(QWidget *parent = nullptr, Qt::WindowFlags f = Qt::WindowFlags());

public slots:
    virtual int exec();
    virtual int exec(int &);
};

#endif // DBDIALOG_H
