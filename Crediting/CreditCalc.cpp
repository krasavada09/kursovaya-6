#include "CreditCalc.h"

double getPaymentAmount(double amount, double rate, int term) {
// Функция подсчета размера аннуитетного платежа
// Принимает сумму кредита, годовую ставку (десятичная дробь), срок кредитования
// Возвращает размер платежа
    rate /= 12;
    return amount * (rate + rate / (qPow(1 + rate, term) - 1));
}
