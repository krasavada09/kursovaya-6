#ifndef REGDIALOG_H
#define REGDIALOG_H

#include <QtWidgets>
#include <QtSql>
#include "NameValidator.h"
#include "EmailValidator.h"

// Класс диалога регистрации
// Реализует регистрацию пользователя

class RegDialog : public QDialog {
    Q_OBJECT
private:
    QLineEdit *m_pedtLogin;                     // Поля диалога
    QLineEdit *m_pedtPassword;
    QLineEdit *m_pedtReplyPassword;
    QLineEdit *m_pedtLName;
    QLineEdit *m_pedtFName;
    QLineEdit *m_pedtSName;
    QLineEdit *m_pedtEmail;

    NameValidator m_nameValidator;                 // Валидаторы данных
    EmailValidator m_emailValidator;

    QLabel *m_plblLogin;                        // Подписи диалога
    QLabel *m_plblPassword;
    QLabel *m_plblReplyPassword;
    QLabel *m_plblLName;
    QLabel *m_plblFName;
    QLabel *m_plblSName;
    QLabel *m_plblEmail;

    QPushButton *m_pbtnReg;                     // Кнопки диалога
    QPushButton *m_pbtnCancel;

    QFormLayout *m_pfrmLayout;                  // Компоновки диалога
    QHBoxLayout *m_pbtnLayout;
    QVBoxLayout *m_playout;

public:
    RegDialog(QWidget *parent = nullptr);

    void clear();
    bool formIsValid();

public slots:
    virtual int exec();
    virtual void accept();
};

#endif // REGDIALOG_H
