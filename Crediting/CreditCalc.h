#ifndef CREDITCALC_H
#define CREDITCALC_H

#include <QtMath>

// Функции для расчета данных кредитного договора

double getPaymentAmount(double amount, double rate, int term);

#endif // CREDITCALC_H
