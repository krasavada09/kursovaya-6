#ifndef PAYMENTSCHEDULESWIDGET_H
#define PAYMENTSCHEDULESWIDGET_H

#include "AbstractEntityWidget.h"

// Класс виджета графика платежей
// Описывает визуальное представление

class PaymentSchedulesWidget : public AbstractEntityWidget {
    Q_OBJECT
private:
    QHBoxLayout *m_pbtnLayout;                      // Компоновки виджета
    QVBoxLayout *m_playout;

public:
    PaymentSchedulesWidget(QWidget *parent = nullptr);

    virtual void setContextMenu();
};

#endif // PAYMENTSCHEDULESWIDGET_H
