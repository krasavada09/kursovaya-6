#include "UsersBaseDialog.h"

UsersBaseDialog::UsersBaseDialog(QWidget *parent)
    : DbDialog(parent,
              Qt::CustomizeWindowHint |
              Qt::WindowTitleHint |
              Qt::WindowCloseButtonHint |
              Qt::WindowMaximizeButtonHint) {
    setModal(true);

    m_pcbbGroupID = new QComboBox;
    m_pedtLName = new QLineEdit;
    m_pedtFName = new QLineEdit;
    m_pedtSName = new QLineEdit;
    m_pedtLogin = new QLineEdit;
    m_pedtEmail = new QLineEdit;

    m_pedtLName->setMaxLength(30);
    m_pedtFName->setMaxLength(30);
    m_pedtSName->setMaxLength(30);
    m_pedtEmail->setMaxLength(30);

    m_pedtLName->setValidator(&m_nameValidator);
    m_pedtFName->setValidator(&m_nameValidator);
    m_pedtSName->setValidator(&m_nameValidator);
    m_pedtEmail->setValidator(&m_emailValidator);

    m_plblGroupID = new QLabel("Группа");
    m_plblLName = new QLabel("Фамилия *");
    m_plblFName = new QLabel("Имя *");
    m_plblSName = new QLabel("Отчество");
    m_plblLogin = new QLabel("Логин");
    m_plblEmail = new QLabel("Email");

    // Настройка компоновки
    m_pfrmLayout = new QFormLayout;
    m_pfrmLayout->addRow(m_plblGroupID, m_pcbbGroupID);
    m_pfrmLayout->addRow(m_plblLName, m_pedtLName);
    m_pfrmLayout->addRow(m_plblFName, m_pedtFName);
    m_pfrmLayout->addRow(m_plblSName, m_pedtSName);
    m_pfrmLayout->addRow(m_plblEmail, m_pedtEmail);
    m_pfrmLayout->addRow(m_plblLogin, m_pedtLogin);

    resize(350, height());
}

void UsersBaseDialog::clear() {
// Возвращает форму в начальное состояние
    m_pcbbGroupID->setCurrentIndex(0);
    m_pedtLName->setText("");
    m_pedtFName->setText("");
    m_pedtSName->setText("");
    m_pedtLogin->setText("");
    m_pedtEmail->setText("");

    m_pcbbGroupID->setFocus();

    // Заполнение списка "Группа"
    m_pcbbGroupID->clear();
    QSqlQuery query;
    query.exec("SELECT id, name FROM Groups");
    m_pcbbGroupID->addItem("Без группы", "");
    while (query.next()) {
        m_pcbbGroupID->addItem(
                    query.value("name").toString(),
                    query.value("id").toInt()
                    );
    }
}

int UsersBaseDialog::exec() {
// Исполнение диалога
    clear();

    return QDialog::exec();
}

int UsersBaseDialog::exec(int &id) {
// Исполнение диалога
    clear();

    // Валидация id модели
    if (!id) return QDialog::Rejected;
    m_id = id;

    QSqlQuery query;
    query.prepare(
                "SELECT "
                "u.id, u.group_id, g.name AS group_name, "
                "u.last_name, u.first_name, u.second_name, "
                "u.login, u.password, u.email "
                "FROM Users u "
                "LEFT JOIN Groups g ON (u.group_id = g.id) "
                "WHERE u.id = :id;"
                );
    query.bindValue(":id", m_id);
    if (!query.exec() || query.size() > 1) {
        QMessageBox::critical(this,
                              "Ошибка чтения",
                              "Не удалось считать данные пользователя",
                              QMessageBox::Ok
                              );
        return QDialog::Rejected;
    }

    query.first();

    // Вывод данных в поля диалога
    m_pcbbGroupID->setCurrentText(query.value("group_name").toString());
    m_pedtLName->setText(query.value("last_name").toString());
    m_pedtFName->setText(query.value("first_name").toString());
    m_pedtSName->setText(query.value("second_name").toString());
    m_pedtEmail->setText(query.value("email").toString());
    m_pedtLogin->setText(query.value("login").toString());

    return QDialog::exec();
}

bool UsersBaseDialog::formIsValid() {
// Возвращает true, если все поля формы валидны
// Возвращает false иначе
    QString input; int pos = 0;

    input = m_pedtLName->text();
    if (m_nameValidator.validate(input, pos) != QValidator::Acceptable) return false;
    input = m_pedtFName->text();
    if (m_nameValidator.validate(input, pos) != QValidator::Acceptable) return false;
    input = m_pedtSName->text();
    if (m_nameValidator.validate(input, pos) != QValidator::Acceptable
        && !input.isEmpty()) return false;
    input = m_pedtEmail->text();
    if (m_emailValidator.validate(input, pos) != QValidator::Acceptable
        && !input.isEmpty()) return false;

    return true;
}
