#ifndef CREDITCONTRACTSDELDIALOG_H
#define CREDITCONTRACTSDELDIALOG_H

#include "DbDialog.h"

// Класс диалога удаления договора
// Предназначен для удаления данных из БД

class CreditContractsDelDialog : public DbDialog {
    Q_OBJECT
private:
    int m_id;                                // ID договора

    QLabel *m_plbl;                          // Подпись диалога
    QPushButton *m_pbtnYes;                  // Кнопки диалога
    QPushButton *m_pbtnNo;

    QHBoxLayout *m_pbtnLayout;               // Компоновки диалога
    QVBoxLayout *m_playout;

public:
    CreditContractsDelDialog(QWidget *parent = nullptr);

public slots:
    virtual int exec(int &id);
    virtual void accept();
};

#endif // CREDITCONTRACTSDELDIALOG_H
