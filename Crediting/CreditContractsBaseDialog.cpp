#include "CreditContractsBaseDialog.h"

CreditContractsBaseDialog::CreditContractsBaseDialog(QWidget *parent)
    : DbDialog(parent,
               Qt::CustomizeWindowHint |
               Qt::WindowTitleHint |
               Qt::WindowCloseButtonHint |
               Qt::WindowMaximizeButtonHint) {
    setModal(true);

    m_pcbbProvisionID = new QComboBox;
    m_pcbbPurposeID = new QComboBox;
    m_pcbbRepaymentMethodID = new QComboBox;
    m_pcbbFormID = new QComboBox;
    m_pcbbCustomerID = new QComboBox;
    m_pedtAmount = new QLineEdit;
    m_pedtRate = new QLineEdit;
    m_pedtTerm = new QLineEdit;
    m_pdteDate = new QDateEdit;
    m_pedtInsuranceContractID = new QLineEdit;

    m_pdteDate->setCalendarPopup(true);

    m_dblValidator.setBottom(1);
    m_dblValidator.setDecimals(2);
    m_dblValidator.setNotation(QDoubleValidator::StandardNotation);

    m_pedtAmount->setValidator(&m_dblValidator);
    m_pedtRate->setValidator(&m_dblValidator);

    m_intValidator.setBottom(0);
    m_pedtTerm->setValidator(&m_intValidator);
    m_pedtInsuranceContractID->setValidator(&m_intValidator);

    m_plblProvisionID = new QLabel("Обеспечение *");
    m_plblPurposeID = new QLabel("Цель *");
    m_plblRepaymentMethodID = new QLabel("Метод погашения *");
    m_plblFormID = new QLabel("Форма выдачи *");
    m_plblCustomerID = new QLabel("Клиент *");
    m_plblAmount = new QLabel("Сумма *");
    m_plblRate = new QLabel("Ставка *");
    m_plblTerm = new QLabel("Срок *");
    m_plblDate = new QLabel("Дата выдачи *");
    m_plblInsuranceContractID = new QLabel("Номер договора страхования");

    // Настройка компоновки
    m_pfrmLayout = new QFormLayout;
    m_pfrmLayout->addRow(m_plblProvisionID, m_pcbbProvisionID);
    m_pfrmLayout->addRow(m_plblPurposeID, m_pcbbPurposeID);
    m_pfrmLayout->addRow(m_plblRepaymentMethodID, m_pcbbRepaymentMethodID);
    m_pfrmLayout->addRow(m_plblFormID, m_pcbbFormID);
    m_pfrmLayout->addRow(m_plblCustomerID, m_pcbbCustomerID);
    m_pfrmLayout->addRow(m_plblAmount, m_pedtAmount);
    m_pfrmLayout->addRow(m_plblRate, m_pedtRate);
    m_pfrmLayout->addRow(m_plblTerm, m_pedtTerm);
    m_pfrmLayout->addRow(m_plblDate, m_pdteDate);
    m_pfrmLayout->addRow(m_plblInsuranceContractID, m_pedtInsuranceContractID);
}

void CreditContractsBaseDialog::clear() {
// Возвращает форму в начальное состояние
    m_pedtAmount->setText("");
    m_pedtRate->setText("");
    m_pedtTerm->setText("");
    m_pdteDate->setDate(QDate::currentDate());
    m_pedtInsuranceContractID->setText("");

    m_pcbbProvisionID->setFocus();

    m_pcbbProvisionID->clear();
    QSqlQuery query;
    query.exec("SELECT * FROM Provisions");
    while (query.next()) {
        m_pcbbProvisionID->addItem(
                    query.value("name").toString(),
                    query.value("id").toInt()
                    );
    }

    m_pcbbPurposeID->clear();
    query.exec("SELECT * FROM Purposes");
    while (query.next()) {
        m_pcbbPurposeID->addItem(
                    query.value("name").toString(),
                    query.value("id").toInt()
                    );
    }

    m_pcbbRepaymentMethodID->clear();
    query.exec("SELECT * FROM Repayment_Methods");
    while (query.next()) {
        m_pcbbRepaymentMethodID->addItem(
                    query.value("name").toString(),
                    query.value("id").toInt()
                    );
    }

    m_pcbbFormID->clear();
    query.exec("SELECT * FROM Forms");
    while (query.next()) {
        m_pcbbFormID->addItem(
                    query.value("name").toString(),
                    query.value("id").toInt()
                    );
    }

    m_pcbbCustomerID->clear();
    query.exec("SELECT id, "
               "last_name || ' ' || "
               "first_name || ' ' || "
               "second_name AS name,"
               "birth_date "
               "FROM Customers "
               "ORDER BY name;");
    while (query.next()) {
        m_pcbbCustomerID->addItem(
                    query.value("name").toString() + " - " +
                        query.value("birth_date").toDate().toString("dd.MM.yyyy"),
                    query.value("id").toInt()
                    );
    }
}

int CreditContractsBaseDialog::exec() {
// Исполнение диалога
    clear();

    return QDialog::exec();
}

int CreditContractsBaseDialog::exec(int &id) {
// Исполнение диалога
    clear();

    // Валидация id модели
    if (!id) return QDialog::Rejected;
    m_id = id;

    QSqlQuery query;
    query.prepare(
                "SELECT "
                "pr.name AS provision_name, pp.name AS purpose_name, "
                "rm.name AS repayment_method_name, f.name AS form_name, "
                "c.last_name || ' ' || "
                "c.first_name || ' ' || "
                "c.second_name AS customer_name, "
                "cc.amount, cc.rate, cc.term, cc.date, "
                "cc.insurance_contract_id "
                "FROM Credit_Contracts cc "
                "JOIN Users u ON cc.user_id = u.id "
                "JOIN Provisions pr ON cc.provision_id = pr.id "
                "JOIN Purposes pp ON cc.purpose_id = pp.id "
                "JOIN Repayment_Methods rm ON cc.repayment_method_id = rm.id "
                "JOIN Forms f ON cc.form_id = f.id "
                "JOIN Customers c ON cc.customer_id = c.id "
                "WHERE cc.id = :id;"
                );
    query.bindValue(":id", m_id);
    if (!query.exec() || query.size() > 1) {
        QMessageBox::critical(this,
                              "Ошибка чтения",
                              "Не удалось считать данные договора",
                              QMessageBox::Ok
                              );
        return QDialog::Rejected;
    }

    query.first();

    m_pcbbProvisionID->setCurrentText(query.value("provision_name").toString());
    m_pcbbPurposeID->setCurrentText(query.value("purpose_name").toString());
    m_pcbbRepaymentMethodID->setCurrentText(
                query.value("repayment_method_name").toString()
                );
    m_pcbbFormID->setCurrentText(query.value("form_name").toString());
    m_pcbbCustomerID->setCurrentText(query.value("customer_name").toString());
    QLocale locale(QLocale::Russian);
    m_pedtAmount->setText(locale.toString(query.value("amount").toDouble(), 'f', 2));
    m_pedtRate->setText(locale.toString(query.value("rate").toDouble(), 'f', 2));
    m_pedtTerm->setText(query.value("term").toString());
    m_pdteDate->setDate(query.value("date").toDate());
    m_pedtInsuranceContractID->setText(
                query.value("insurance_contract_id").toString() == "0" ?
                    "" :
                    query.value("insurance_contract_id").toString()
                );

    return QDialog::exec();
}

bool CreditContractsBaseDialog::formIsValid() {
// Возвращает true, если все поля формы валидны
// Возвращает false иначе
    QString input; int pos = 0;

    input = m_pedtAmount->text();
    if (m_dblValidator.validate(input, pos) != QValidator::Acceptable) return false;
    input = m_pedtRate->text();
    if (m_dblValidator.validate(input, pos) != QValidator::Acceptable) return false;
    input = m_pedtTerm->text();
    if (m_intValidator.validate(input, pos) != QValidator::Acceptable) return false;
    input = m_pedtInsuranceContractID->text();
    if (m_intValidator.validate(input, pos) != QValidator::Acceptable
            && !input.isEmpty()) return false;

    return true;
}
