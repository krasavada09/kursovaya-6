#ifndef CREDITCONTRACTSEDITDIALOG_H
#define CREDITCONTRACTSEDITDIALOG_H

#include "CreditContractsBaseDialog.h"

// Класс диалога изменения договора
// Предназначен для изменения данных в БД

class CreditContractsEditDialog : public CreditContractsBaseDialog {
    Q_OBJECT
private:
    QPushButton *m_pbtnSave;                    // Кнопки диалога
    QPushButton *m_pbtnCancel;

    QHBoxLayout *m_pbtnLayout;                  // Компоновки диалога
    QVBoxLayout *m_playout;

public:
    CreditContractsEditDialog(QWidget *parent = nullptr);

public slots:
    virtual void accept();
};

#endif // CREDITCONTRACTSEDITDIALOG_H
