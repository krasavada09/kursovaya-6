#include "MainWindow.h"

MainWindow::MainWindow(User *user, QWidget *parent)
    : QMainWindow(parent),
      m_pUser(user) {
    setWindowTitle("Кредитование");

    m_pwgt = new QWidget(this);
    setCentralWidget(m_pwgt);

    m_ptableNames = new QVector<QString>;
    setTables();

    m_pentities = new QVector<AbstractEntityWidget *>;
    setEntities();

    m_pexportDialog = new ExportDialog(this, m_pentities, m_ptableNames);
    m_pcreditRepDialog = new CreditsReportDialog(m_pUser, this);
    m_pdebtorRep = new DebtorsReport(this);
    m_pprofitRepDialog = new ProfitReportDialog(this);
    m_ppaymentSchRepDialog = new PaymentScheduleReportDialog(this);

    m_paboutBox = new AboutMessageBox(QPixmap("images/bank-svgrepo-com.svg"),
                                      "О программе",
                                      "<h1 style='color: #3e8088'>"
                                      "Кредитование</h1><br>"
                                      "<b>Версия 1.0</b><br><br>"
                                      "Copyright(c) 2020, Захаров Максим.<br>"
                                      "All Right Reserved.",
                                      QMessageBox::NoButton,
                                      this
                                      );

    setMenu();
    menuBar()->addMenu(m_pmnuTable);
    menuBar()->addMenu(m_pmnuReport);
    menuBar()->addMenu(m_pmnuUser);
    menuBar()->addMenu(m_pmnuAbout);

    setLayout();
}

void MainWindow::setTables() {
// Устанавливает таблицы
    if (m_pUser->x()) {
        m_ptableNames->append("Пользователи");
        m_ptableNames->append("Группы");
    }

    m_ptableNames->append("Клиенты");
    m_ptableNames->append("Договоры кредитования");
    m_ptableNames->append("Графики платежей");
    m_ptableNames->append("Платежи");
}

void MainWindow::setMenu() {
// Устанавливает меню
    m_pmnuTable = new QMenu("&Таблица");
    m_pmnuReport = new QMenu("&Отчет");
    m_pmnuUser = new QMenu("&Пользователи");
    m_pmnuAbout = new QMenu("&Справка");

    for (int i = 0; i < m_ptableNames->size(); i++) {
        m_pmnuTable->addAction(
                    m_ptableNames->at(i),
                    [this, i]() {m_playout->setCurrentIndex(i);}
        );
    }
    m_pmnuTable->addSeparator();
    m_pmnuTable->addAction(QIcon("images/solid/file-export.svg"),
                           "Экспорт...",
                           m_pexportDialog, SLOT(exec())
                           );
    m_pmnuTable->addSeparator();
    m_pmnuTable->addAction(QIcon("images/solid/sign-out-alt.svg"),
                           "Выход",
                           &QApplication::quit
                           );

    m_pmnuReport->addAction("Выданные кредиты...", m_pcreditRepDialog, SLOT(exec()));
    m_pmnuReport->addAction("Должники", m_pdebtorRep, SLOT(exec()));
    m_pmnuReport->addAction("Прибыль банка...", m_pprofitRepDialog, SLOT(exec()));
    m_pmnuReport->addAction("График платежей...",
                            m_ppaymentSchRepDialog, SLOT(exec())
                            );

    m_pmnuUser->addAction(QIcon("images/solid/user.svg"), m_pUser->login());

    m_pmnuAbout->addAction(QIcon("images/solid/info.svg"),
                           "О программе", m_paboutBox, SLOT(exec())
                           );

}

void MainWindow::setEntities() {
// Устанавливает сущности
    QString queryString;
    int index;
    index = m_ptableNames->indexOf("Пользователи");
    if (index != -1) {
        m_pentities->insert(
                    m_ptableNames->indexOf("Пользователи"), new EntityWidget(this)
                    );
        m_pentities->at(index)->model()->setQuery(
                "SELECT "
                "u.id, g.name AS Группа, "
                "u.last_name AS Фамилия, u.first_name AS Имя, "
                "u.second_name AS Отчество, "
                "u.login AS Логин, u.email AS \"Email\""
                "FROM Users u "
                "LEFT JOIN Groups g ON (u.group_id = g.id) "
                "ORDER BY g.id, u.last_name, u.first_name, u.second_name;"
                );
        m_pentities->at(index)->view()->hideColumn(0);
        m_pentities->at(index)->setShowDialog(new UsersShowDialog(this));
        m_pentities->at(index)->setAddDialog(new UsersAddDialog(this));
        m_pentities->at(index)->setEditDialog(new UsersEditDialog(this));
        m_pentities->at(index)->setDelDialog(new UsersDelDialog(this));
        m_pentities->at(index)->setContextMenu();
        m_pentities->at(index)->resize();
    }

    index = m_ptableNames->indexOf("Группы");
    if (index != -1) {
        m_pentities->insert(
                    m_ptableNames->indexOf("Группы"), new EntityWidget(this)
                    );
        m_pentities->at(index)->model()->setQuery(
                "SELECT "
                "id, name AS Название, "
                "r AS Просмотр, w AS Редактирование, x AS \"Полный доступ\" "
                "FROM Groups "
                "ORDER BY id;"
                );
        m_pentities->at(index)->view()->hideColumn(0);
        m_pentities->at(index)->setShowDialog(new GroupsShowDialog(this));
        m_pentities->at(index)->setAddDialog(new GroupsAddDialog(this));
        m_pentities->at(index)->setEditDialog(new GroupsEditDialog(this));
        m_pentities->at(index)->setDelDialog(new GroupsDelDialog(this));
        m_pentities->at(index)->setContextMenu();
        m_pentities->at(index)->resize();
    }

    index = m_ptableNames->indexOf("Клиенты");
    if (index != -1) {
        m_pentities->insert(
                    m_ptableNames->indexOf("Клиенты"), new EntityWidget(this)
                    );
        m_pentities->at(index)->model()->setQuery(
                "SELECT "
                "c.id, c.passport AS Паспорт, "
                "c.last_name AS Фамилия, c.first_name AS Имя, "
                "c.second_name AS Отчество, "
                "c.sex AS Пол, c.birth_date AS \"Дата рождения\", "
                "c.phone AS Телефон, c.address AS Адрес, "
                "c.email AS \"Email\", r.name AS \"Кредитный рейтинг\" "
                "FROM Customers c "
                "LEFT JOIN Credit_Ratings r ON (c.credit_rating_id = r.id) "
                "ORDER BY c.last_name, c.first_name, c.second_name;"
                );
        m_pentities->at(index)->view()->hideColumn(0);
        m_pentities->at(index)->setShowDialog(new CustomersShowDialog(this));
        m_pentities->at(index)->setAddDialog(new CustomersAddDialog(this));
        m_pentities->at(index)->setEditDialog(new CustomersEditDialog(this));
        m_pentities->at(index)->setDelDialog(new CustomersDelDialog(this));
        m_pentities->at(index)->setContextMenu();
        m_pentities->at(index)->resize();
    }

    index = m_ptableNames->indexOf("Договоры кредитования");
    if (index != -1) {
        m_pentities->insert(
                    m_ptableNames->indexOf("Договоры кредитования"),
                    new EntityWidget(this)
                    );
        queryString = "SELECT "
                      "cc.id, "
                      "cc.id AS №, "
                      "u.last_name || ' ' || "
                      "u.first_name || ' ' || "
                      "u.second_name AS Сотрудник, "
                      "pr.name AS Обеспечение, pp.name AS Цель, "
                      "rm.name AS \"Метод погашения\", f.name AS \"Форма выдачи\", "
                      "c.last_name || ' ' || "
                      "c.first_name || ' ' || "
                      "c.second_name AS Клиент, "
                      "cc.amount AS Сумма, cc.rate AS Ставка, cc.term AS Срок, "
                      "cc.date AS \"Дата выдачи\", "
                      "cc.insurance_contract_id AS \"Договор страхования\" "
                      "FROM Credit_Contracts cc "
                      "JOIN Users u ON cc.user_id = u.id "
                      "JOIN Provisions pr ON cc.provision_id = pr.id "
                      "JOIN Purposes pp ON cc.purpose_id = pp.id "
                      "JOIN Repayment_Methods rm ON cc.repayment_method_id = rm.id "
                      "JOIN Forms f ON cc.form_id = f.id "
                      "JOIN Customers c ON cc.customer_id = c.id ";
        if (!m_pUser->w())
            queryString += "WHERE u.id = '" + QString::number(m_pUser->id()) + "' ";
        queryString += "ORDER BY cc.id DESC;";
        m_pentities->at(index)->model()->setQuery(queryString);
        m_pentities->at(index)->view()->hideColumn(0);
        m_pentities->at(index)->setShowDialog(new CreditContractsShowDialog(this));
        m_pentities->at(index)->setAddDialog(
                    new CreditContractsAddDialog(m_pUser->id(), this)
                    );
        m_pentities->at(index)->setEditDialog(new CreditContractsEditDialog(this));
        m_pentities->at(index)->setDelDialog(new CreditContractsDelDialog(this));
        m_pentities->at(index)->setContextMenu();
        m_pentities->at(index)->resize();
    }

    index = m_ptableNames->indexOf("Графики платежей");
    if (index != -1) {
        m_pentities->insert(
                    m_ptableNames->indexOf("Графики платежей"),
                    new PaymentSchedulesWidget(this)
                    );
        queryString = "WITH Payment_Schedules_Current AS ( "
                      "SELECT "
                      "MAX(id) AS id, credit_contract_id "
                      "FROM Payment_Schedules "
                      "GROUP BY credit_contract_id "
                      ") "
                      "SELECT "
                      "si.id, "
                      "psc.credit_contract_id AS \"№ Договора\", si.date AS Дата, "
                      "si.amount AS Сумма "
                      "FROM Schedule_Items si "
                      "JOIN Payment_Schedules_Current psc "
                      "ON (psc.id = si.payment_schedule_id) "
                      "JOIN Credit_Contracts cc ON (cc.id = psc.credit_contract_id) "
                      "JOIN Users u ON (u.id = cc.user_id) ";
        if (!m_pUser->w())
            queryString += "WHERE u.id = '" + QString::number(m_pUser->id()) + "' ";
        queryString += "ORDER BY cc.id DESC, si.date;";
        m_pentities->at(index)->model()->setQuery(queryString);
        m_pentities->at(index)->view()->hideColumn(0);
        m_pentities->at(index)->setAddDialog(new SchedulesAddDialog(this));
        m_pentities->at(index)->setContextMenu();
        m_pentities->at(index)->resize();
    }

    index = m_ptableNames->indexOf("Платежи");
    if (index != -1) {
        m_pentities->insert(
                    m_ptableNames->indexOf("Платежи"),
                    new PaymentsWidget(this)
                    );
        queryString = "SELECT "
                      "p.id, p.credit_contract_id AS \"№ Договора\", "
                      "p.date::date AS Дата, p.amount AS Сумма "
                      "FROM Payments p "
                      "JOIN Credit_Contracts cc ON (cc.id = p.credit_contract_id) "
                      "JOIN Users u ON (u.id = cc.user_id) ";
        if (!m_pUser->w())
            queryString += "WHERE u.id = '" + QString::number(m_pUser->id()) + "' ";
        queryString += "ORDER BY p.date DESC;";
        m_pentities->at(index)->model()->setQuery(queryString);
        m_pentities->at(index)->view()->hideColumn(0);
        m_pentities->at(index)->setContextMenu();
        m_pentities->at(index)->resize();
    }
}

void MainWindow::setLayout() {
// Устанавливает компоновку
    m_playout = new QStackedLayout;
    for (int i = 0; i < m_ptableNames->size(); i++) {
        m_playout->addWidget(m_pentities->at(i));
    }

    m_pwgt->setLayout(m_playout);
}

void MainWindow::show() {
// Открытие формы
    for (int i = 0; i < m_pentities->size(); i++) {
        m_pentities->at(i)->refresh();
    }

    QMainWindow::show();
}
