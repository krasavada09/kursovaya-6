QT       += core gui

QT += sql
QT += axcontainer

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    AboutMessageBox.cpp \
    AbstractEntityWidget.cpp \
    AxFuntions.cpp \
    CreditCalc.cpp \
    CreditContractsAddDialog.cpp \
    CreditContractsBaseDialog.cpp \
    CreditContractsDelDialog.cpp \
    CreditContractsEditDialog.cpp \
    CreditContractsShowDialog.cpp \
    CreditsReportDialog.cpp \
    CustomersAddDialog.cpp \
    CustomersDelDialog.cpp \
    CustomersEditDialog.cpp \
    CustomersShowDialog.cpp \
    DatabaseConnection.cpp \
    DbDialog.cpp \
    DebtorsReport.cpp \
    EmailValidator.cpp \
    EntityFindDialog.cpp \
    EntityWidget.cpp \
    ExportDialog.cpp \
    GroupsAddDialog.cpp \
    GroupsBaseDialog.cpp \
    GroupsDelDialog.cpp \
    GroupsEditDialog.cpp \
    GroupsShowDialog.cpp \
    MainWindow.cpp \
    NameValidator.cpp \
    PaymentScheduleReportDialog.cpp \
    PaymentSchedulesWidget.cpp \
    PaymentsWidget.cpp \
    ProfitReportDialog.cpp \
    SchedulesAddDialog.cpp \
    StyledSortFilterProxyModel.cpp \
    User.cpp \
    UsersAddDialog.cpp \
    UsersBaseDialog.cpp \
    UsersDelDialog.cpp \
    UsersEditDialog.cpp \
    UsersShowDialog.cpp \
    main.cpp \
    AuthDialog.cpp \
    CustomersBaseDialog.cpp \
    RegDialog.cpp \

HEADERS += \
    AboutMessageBox.h \
    AbstractEntityWidget.h \
    AuthDialog.h \
    AxFuntions.h \
    CreditCalc.h \
    CreditContractsAddDialog.h \
    CreditContractsBaseDialog.h \
    CreditContractsDelDialog.h \
    CreditContractsEditDialog.h \
    CreditContractsShowDialog.h \
    CreditsReportDialog.h \
    CustomersAddDialog.h \
    CustomersBaseDialog.h \
    CustomersDelDialog.h \
    CustomersEditDialog.h \
    CustomersShowDialog.h \
    DatabaseConnection.h \
    DbDialog.h \
    DebtorsReport.h \
    EmailValidator.h \
    EntityFindDialog.h \
    EntityWidget.h \
    ExportDialog.h \
    GroupsAddDialog.h \
    GroupsBaseDialog.h \
    GroupsDelDialog.h \
    GroupsEditDialog.h \
    GroupsShowDialog.h \
    MainWindow.h \
    NameValidator.h \
    PaymentScheduleReportDialog.h \
    PaymentSchedulesWidget.h \
    PaymentsWidget.h \
    ProfitReportDialog.h \
    RegDialog.h \ \
    SchedulesAddDialog.h \
    StyledSortFilterProxyModel.h \
    User.h \
    UsersAddDialog.h \
    UsersBaseDialog.h \
    UsersDelDialog.h \
    UsersEditDialog.h \
    UsersShowDialog.h

FORMS += \

RC_ICONS = images/appicon.ico

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
