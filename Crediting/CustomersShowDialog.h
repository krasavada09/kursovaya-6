#ifndef CUSTOMERSSHOWDIALOG_H
#define CUSTOMERSSHOWDIALOG_H

#include "CustomersBaseDialog.h"

// Класс диалога просмотра клиента
// Предназначен для вывода информации из БД

class CustomersShowDialog : public CustomersBaseDialog {
    Q_OBJECT
private:
    QPushButton *m_pbtnOk;                      // Кнопка диалога

    QHBoxLayout *m_pbtnLayout;                  // Компоновки диалога
    QVBoxLayout *m_playout;

public:
    CustomersShowDialog(QWidget *parent = nullptr);
};

#endif // CUSTOMERSSHOWDIALOG_H
