#include "User.h"

User::User() {
    m_pID = new int;
    m_plast_name = new QString;
    m_pfirst_name = new QString;
    m_psecond_name = new QString;
    m_plogin = new QString;
    m_pr = new bool;
    m_pw = new bool;
    m_px = new bool;
}

User::User(int id,
           QString last_name,
           QString first_name,
           QString second_name,
           QString login,
           bool r, bool w, bool x)
    : User() {
    setUser(id, last_name, first_name, second_name, login, r, w, x);
}

User::~User() {
    delete m_pID;
    delete m_plast_name;
    delete m_pfirst_name;
    delete m_psecond_name;
    delete m_plogin;
    delete m_pr;
    delete m_pw;
    delete m_px;
}

void User::setUser(
        int id,
        QString last_name, QString first_name, QString second_name,
        QString login,
        bool r, bool w, bool x) {
// Устанавливает пользователя
    *m_pID = id;
    *m_plast_name = last_name;
    *m_pfirst_name = first_name;
    *m_psecond_name = second_name;
    *m_plogin = login;
    *m_pr = r;
    *m_pw = w;
    *m_px = x;
}

int User::id() {
// Возвращает id пользователя
    return *m_pID ? *m_pID : 0;
}

QString User::login() {
// Возвращает login пользователя
    return *m_plogin;
}

bool User::r() {
// Возвращает права пользователя
    return *m_pr;
}

bool User::w() {
// Возвращает права пользователя
    return *m_pw;
}

bool User::x() {
// Возвращает права пользователя
    return *m_px;
}
