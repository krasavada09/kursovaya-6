#ifndef PROFITREPORTDIALOG_H
#define PROFITREPORTDIALOG_H

#include <QtWidgets>
#include <QtSql>
#include <QAxBase>
#include <QAxObject>
#include "AxFuntions.h"

// Диалог отчета "Прибыль банка"
// Предназначен для формирования отчета

class ProfitReportDialog : public QDialog {
    Q_OBJECT
private:
    QDateEdit *m_pdteFirstDate;                 // Поля диалога
    QDateEdit *m_pdteLastDate;

    QLabel *m_plblFirstDate;                    // Подписи диалога
    QLabel *m_plblLastDate;

    QPushButton *m_pbtnReport;                  // Кнопки диалога
    QPushButton *m_pbtnCancel;

    QFormLayout *m_pfrmLayout;                  // Компоновки диалога
    QHBoxLayout *m_pbtnLayout;
    QVBoxLayout *m_playout;

public:
    ProfitReportDialog(QWidget *parent = nullptr);

    void clear();

public slots:
    virtual int exec();
    virtual void accept();
};

#endif // PROFITREPORTDIALOG_H
