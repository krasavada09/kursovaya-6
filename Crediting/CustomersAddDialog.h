#ifndef CUSTOMERSADDDIALOG_H
#define CUSTOMERSADDDIALOG_H

#include "CustomersBaseDialog.h"

// Класс диалога добавления клиента
// Предназначен для внесения данных в БД

class CustomersAddDialog : public CustomersBaseDialog {
    Q_OBJECT
private:
    QPushButton *m_pbtnAdd;                             // Кнопки диалога
    QPushButton *m_pbtnCancel;

    QHBoxLayout *m_pbtnLayout;                          // Компоновки диалога
    QVBoxLayout *m_playout;

public:
    CustomersAddDialog(QWidget *parent = nullptr);

public slots:
    virtual void accept();
};

#endif // CUSTOMERSADDDIALOG_H
