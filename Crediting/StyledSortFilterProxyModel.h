#ifndef STYLEDSORTFILTERPROXYMODEL_H
#define STYLEDSORTFILTERPROXYMODEL_H

#include <QSortFilterProxyModel>
#include <QColor>
#include <QBrush>
#include <QLocale>

// Класс промежуточной модели
// Реализует подсветку строк

class StyledSortFilterProxyModel : public QSortFilterProxyModel {
    Q_OBJECT
private:
    QList<int> *m_pidList;      // ID записей для подсветки

public:
    StyledSortFilterProxyModel(QObject *parent = nullptr);

    void setIDs(QList<int> idList);
    void clearIDs();
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
};

#endif // STYLEDSORTFILTERPROXYMODEL_H
