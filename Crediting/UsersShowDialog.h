#ifndef USERSSHOWDIALOG_H
#define USERSSHOWDIALOG_H

#include "UsersBaseDialog.h"

// Класс диалога просмотра пользователя
// Предназначен для вывода информации из БД

class UsersShowDialog : public UsersBaseDialog {
    Q_OBJECT
private:
    QPushButton *m_pbtnOk;                      // Кнопка диалога

    QHBoxLayout *m_pbtnLayout;                  // Компоновки диалога
    QVBoxLayout *m_playout;

public:
    UsersShowDialog(QWidget *parent = nullptr);
};

#endif // USERSSHOWDIALOG_H
