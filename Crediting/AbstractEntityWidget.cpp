#include "AbstractEntityWidget.h"

AbstractEntityWidget::AbstractEntityWidget(QWidget *parent)
    : QWidget(parent) {
    m_ptblView = new QTableView;
    m_pqueryModel = new QSqlQueryModel;
    m_pproxyModel = new StyledSortFilterProxyModel;

    m_id = 0;

    m_pproxyModel->setSourceModel(m_pqueryModel);
    m_ptblView->setModel(m_pproxyModel);
    m_ptblView->setSortingEnabled(true);

    m_pmenu = new QMenu;
    m_ptblView->setContextMenuPolicy(Qt::CustomContextMenu);

    // Установка индекса кликом по представлению
    connect(m_ptblView, SIGNAL(clicked(QModelIndex)),
            this, SLOT(setIndex(QModelIndex))
            );
    // Открытие контекстного меню
    connect(m_ptblView, SIGNAL(customContextMenuRequested(const QPoint &)),
            this, SLOT(showContextMenu(const QPoint &))
            );

    m_pbtnShow = new QPushButton(QIcon("images/solid/file-alt.svg"), "Просмотр...");
    m_pbtnAdd = new QPushButton(QIcon("images/solid/plus.svg"), "Добавить...");
    m_pbtnEdit = new QPushButton(QIcon("images/solid/edit.svg"), "Редактировать...");
    m_pbtnDelete = new QPushButton(QIcon("images/solid/trash-alt.svg"), "Удалить");

    m_pbtnFind = new QPushButton(QIcon("images/solid/search.svg"), "");

    m_pbtnFirst = new QPushButton(QIcon("images/solid/angle-double-left.svg"), "");
    m_pbtnPrevious = new QPushButton(QIcon("images/solid/angle-left.svg"), "");
    m_pbtnNext = new QPushButton(QIcon("images/solid/angle-right.svg"), "");
    m_pbtnLast = new QPushButton(QIcon("images/solid/angle-double-right.svg"), "");

    setFindDialog(new EntityFindDialog(this));

    // Навигация по представлению
    connect(m_pbtnFirst, &QPushButton::clicked,
            [this]() {
                setIndex(m_pproxyModel->index(0, 0));
                m_ptblView->selectRow(0);
            });
    connect(m_pbtnPrevious, &QPushButton::clicked,
            [this]() {
                int row = m_index.row() - 1;
                if (row < 0) row = 0;
                setIndex(m_pproxyModel->index(row, 0));
                m_ptblView->selectRow(row);
            });
    connect(m_pbtnNext, &QPushButton::clicked,
            [this]() {
                int row = m_index.row() + 1;
                if (row >= m_pproxyModel->rowCount())
                    row = m_pproxyModel->rowCount() - 1;
                setIndex(m_pproxyModel->index(row, 0));
                m_ptblView->selectRow(row);
            });
    connect(m_pbtnLast, &QPushButton::clicked,
            [this]() {
                setIndex(m_pproxyModel->index(m_pproxyModel->rowCount() - 1, 0));
                m_ptblView->selectRow(m_pproxyModel->rowCount()-1);
            });
}

QSqlQueryModel* AbstractEntityWidget::model() {
// Возвращает модель виджета
    return m_pqueryModel;
}

QTableView* AbstractEntityWidget::view() {
// Возвращает представление виджета
    return m_ptblView;
}

void AbstractEntityWidget::setIndex(QModelIndex index) {
// Устанавливает индекс модели
    m_index = index;

    // Валидация индекса модели
    if (!m_index.isValid()) {
        m_id = 0;
        return;
    }

    QModelIndex indx = m_pproxyModel->index(m_index.row(), 0);
    m_id = m_pproxyModel->data(indx).toInt();
}

void AbstractEntityWidget::refresh() {
// Обновляет модель и представление
    m_pqueryModel->setQuery(m_pqueryModel->query().lastQuery());
    m_ptblView->sortByColumn(-1, Qt::AscendingOrder);

    resize();
}

void AbstractEntityWidget::resize() {
// Подгоняет размер строк и колонок под содержимое ячеек
    m_ptblView->resizeColumnsToContents();
    m_ptblView->resizeRowsToContents();
}

void AbstractEntityWidget::setShowDialog(DbDialog *dialog) {
// Устанавливает окно просмотра
    m_pshowDialog = dialog;

    // Открытие окна кликом по кнопке
    connect(m_pbtnShow, &QPushButton::clicked,
            [this]() {m_pshowDialog->exec(m_id);}
    );
    // Открытие окна двойным кликом по представлению
    connect(m_ptblView, &QTableView::doubleClicked,
            [this]() {
        m_pshowDialog->exec(m_id);
    }
    );
}

void AbstractEntityWidget::setAddDialog(DbDialog *dialog) {
// Устанавливает диалог добавления
    m_paddDialog = dialog;

    // Открытие окна кликом по кнопке
    connect(m_pbtnAdd, SIGNAL(clicked()),
            m_paddDialog, SLOT(exec())
            );

    // Обновление модели после добавления
    connect(m_paddDialog, SIGNAL(accepted()),
            this, SLOT(refresh())
            );
}

void AbstractEntityWidget::setEditDialog(DbDialog *dialog) {
// Устанавливает диалог редактирования
    m_peditDialog = dialog;

    // Открытие окна кликом по кнопке
    connect(m_pbtnEdit, &QPushButton::clicked,
            [this]() {m_peditDialog->exec(m_id);}
    );

    // Обновление модели после изменения
    connect(m_peditDialog, SIGNAL(accepted()),
            this, SLOT(refresh())
            );
}

void AbstractEntityWidget::setDelDialog(DbDialog *dialog) {
// Устанавливает диалог удаления
    m_pdelDialog = dialog;

    // Открытие окна кликом по кнопке
    connect(m_pbtnDelete, &QPushButton::clicked,
            [this]() {m_pdelDialog->exec(m_id);}
    );

    // Обновление модели после удаления
    connect(m_pdelDialog, SIGNAL(accepted()),
            this, SLOT(refresh())
            );
}

void AbstractEntityWidget::setFindDialog(EntityFindDialog *dialog) {
// Устанавливает диалог поиска
    m_pfindDialog = dialog;

    // Открытие окна кликом по кнопке
    connect(m_pbtnFind, &QPushButton::clicked,
            [this]() {m_pfindDialog->exec(m_pproxyModel);}
    );
}

void AbstractEntityWidget::setContextMenu() {
// Устанавливает контекстное меню
    m_pmenu->addAction("Просмотр...", m_pshowDialog, [this]() {
        m_pshowDialog->exec(m_id);
    });
    m_pmenu->addAction("Добавить...", m_paddDialog, SLOT(exec()));
    m_pmenu->addAction("Редактировать...", m_peditDialog, [this]() {
        m_peditDialog->exec(m_id);
    });
    m_pmenu->addAction("Удалить", m_pdelDialog, [this]() {
        m_pdelDialog->exec(m_id);
    });
    m_pmenu->addSeparator();
    m_pmenu->addAction("Поиск", [this]() {
        m_pfindDialog->exec(m_pproxyModel);
    });
    m_pmenu->addAction("Обновить", this, SLOT(refresh()));
}

void AbstractEntityWidget::showContextMenu(const QPoint &pos) {
// Открывает контекстное меню
// Принимает координаты указателя
    QPoint globalPos = m_ptblView->viewport()->mapToGlobal(pos);
    m_pmenu->exec(globalPos);
}
