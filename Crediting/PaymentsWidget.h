#ifndef PAYMENTSWIDGET_H
#define PAYMENTSWIDGET_H

#include "AbstractEntityWidget.h"

// Класс виджета платежей
// Описывает визуальное представление

class PaymentsWidget : public AbstractEntityWidget {
    Q_OBJECT
private:
    QHBoxLayout *m_pbtnLayout;                      // Компоновки виджета
    QVBoxLayout *m_playout;

public:
    PaymentsWidget(QWidget *parent = nullptr);

    virtual void setContextMenu();
};

#endif // PAYMENTSWIDGET_H
