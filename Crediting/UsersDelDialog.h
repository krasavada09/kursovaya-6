#ifndef USERSDELDIALOG_H
#define USERSDELDIALOG_H

#include "DbDialog.h"

// Класс диалога удаления пользователя
// Предназначен для удаления данных из БД

class UsersDelDialog : public DbDialog {
    Q_OBJECT
private:
    int m_id;                                // ID пользователя

    QLabel *m_plbl;                          // Подпись диалога
    QPushButton *m_pbtnYes;                  // Кнопки диалога
    QPushButton *m_pbtnNo;

    QHBoxLayout *m_pbtnLayout;               // Компоновки диалога
    QVBoxLayout *m_playout;

public:
    UsersDelDialog(QWidget *parent = nullptr);

public slots:
    virtual int exec(int &id);
    virtual void accept();
};

#endif // USERSDELDIALOG_H
