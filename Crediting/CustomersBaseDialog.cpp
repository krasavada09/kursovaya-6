#include "CustomersBaseDialog.h"

CustomersBaseDialog::CustomersBaseDialog(QWidget *parent)
    : DbDialog(parent,
              Qt::CustomizeWindowHint |
              Qt::WindowTitleHint |
              Qt::WindowCloseButtonHint |
              Qt::WindowMaximizeButtonHint) {
    setModal(true);

    m_pedtPassport = new QLineEdit;
    m_pedtLName = new QLineEdit;
    m_pedtFName = new QLineEdit;
    m_pedtSName = new QLineEdit;
    m_pcbbSex = new QComboBox;
    m_pdteBDate = new QDateEdit;
    m_pedtPhone = new QLineEdit;
    m_ptxtAddress = new QTextEdit;
    m_pedtEmail = new QLineEdit;
    m_pcbbRatingID = new QComboBox;

    m_pdteBDate->setCalendarPopup(true);

    m_pedtLName->setPlaceholderText("Введите фамилию");
    m_pedtFName->setPlaceholderText("Введите имя");
    m_pedtSName->setPlaceholderText("Введите отчество");
    m_ptxtAddress->setPlaceholderText("Введите адрес прописки");
    m_pedtEmail->setPlaceholderText("Введите email");

    m_pedtPassport->setInputMask("9999-999999");
    m_pedtPhone->setInputMask("+7(999)999-99-99");

    m_pedtLName->setMaxLength(30);
    m_pedtFName->setMaxLength(30);
    m_pedtSName->setMaxLength(30);
    m_pedtEmail->setMaxLength(30);

    m_pedtLName->setValidator(&m_nameValidator);
    m_pedtFName->setValidator(&m_nameValidator);
    m_pedtSName->setValidator(&m_nameValidator);

    m_pedtEmail->setValidator(&m_emailValidator);

    // Заполнение списка "Пол"
    m_pcbbSex->addItem("Мужской", "male");
    m_pcbbSex->addItem("Женский", "female");

    m_ptxtAddress->setTabChangesFocus(true);

    m_plblPassport = new QLabel("Паспорт *");
    m_plblLName = new QLabel("Фамилия *");
    m_plblFName = new QLabel("Имя *");
    m_plblSName = new QLabel("Отчество");
    m_plblSex = new QLabel("Пол *");
    m_plblBDate = new QLabel("Дата рождения *");
    m_plblPhone = new QLabel("Телефон *");
    m_plblAddress = new QLabel("Адрес *");
    m_plblEmail = new QLabel("Email");
    m_plblRatingID = new QLabel("Кредитный рейтинг");

    // Настройка компоновки
    m_pfrmLayout = new QFormLayout;
    m_pfrmLayout->addRow(m_plblPassport, m_pedtPassport);
    m_pfrmLayout->addRow(m_plblLName, m_pedtLName);
    m_pfrmLayout->addRow(m_plblFName, m_pedtFName);
    m_pfrmLayout->addRow(m_plblSName, m_pedtSName);
    m_pfrmLayout->addRow(m_plblSex, m_pcbbSex);
    m_pfrmLayout->addRow(m_plblBDate, m_pdteBDate);
    m_pfrmLayout->addRow(m_plblPhone, m_pedtPhone);
    m_pfrmLayout->addRow(m_plblAddress, m_ptxtAddress);
    m_pfrmLayout->addRow(m_plblEmail, m_pedtEmail);
    m_pfrmLayout->addRow(m_plblRatingID, m_pcbbRatingID);
}

void CustomersBaseDialog::clear() {
// Возвращает форму в начальное состояние
    m_pedtPassport->setText("");
    m_pedtLName->setText("");
    m_pedtFName->setText("");
    m_pedtSName->setText("");
    m_pcbbSex->setCurrentIndex(0);
    m_pdteBDate->setDate(QDate::currentDate());
    m_pedtPhone->setText("");
    m_ptxtAddress->setText("");
    m_pedtEmail->setText("");

    m_pedtPassport->setFocus();

    // Заполнение списка "Рейтинг"
    m_pcbbRatingID->clear();
    QSqlQuery query;
    query.exec("SELECT id, name FROM Credit_Ratings");
    m_pcbbRatingID->addItem("Без рейтинга", "");
    while (query.next()) {
        m_pcbbRatingID->addItem(
                    query.value("name").toString(),
                    query.value("id").toInt()
                    );
    }
}

int CustomersBaseDialog::exec() {
// Исполнение диалога
    clear();

    return QDialog::exec();
}

int CustomersBaseDialog::exec(int &id) {
// Исполнение диалога
    clear();

    // Валидация id модели
    if (!id) return QDialog::Rejected;
    m_id = id;

    // Запрос на выборку данных о клиенте
    QSqlQuery query;
    query.prepare(
                "SELECT "
                "c.id, "
                "c.passport, "
                "c.last_name, c.first_name, c.second_name, "
                "c.sex, c.birth_date, "
                "c.phone, c.address, "
                "c.email, r.name AS credit_rating_name "
                "FROM Customers c "
                "LEFT JOIN Credit_Ratings r ON (c.credit_rating_id = r.id) "
                "WHERE c.id = :id;"
                );
    query.bindValue(":id", m_id);
    if (!query.exec() || query.size() > 1) {
        QMessageBox::critical(this,
                              "Ошибка чтения",
                              "Не удалось считать данные клиента",
                              QMessageBox::Ok
                              );
        return QDialog::Rejected;
    }

    query.first();

    // Вывод данных в поля диалога
    m_pedtPassport->setText(query.value("passport").toString());
    m_pedtLName->setText(query.value("last_name").toString());
    m_pedtFName->setText(query.value("first_name").toString());
    m_pedtSName->setText(query.value("second_name").toString());
    m_pcbbSex->setCurrentText((query.value("sex") == "male") ? "Мужской" : "Женский");
    m_pdteBDate->setDate(query.value("birth_date").toDate());
    m_pedtPhone->setText(query.value("phone").toString());
    m_ptxtAddress->setPlainText(query.value("address").toString());
    m_pedtEmail->setText(query.value("email").toString());
    m_pcbbRatingID->setCurrentText(query.value("credit_rating_name").toString());

    return QDialog::exec();
}

bool CustomersBaseDialog::formIsValid() {
// Возвращает true, если все поля формы валидны
// Возвращает false иначе
    QString input; int pos = 0;

    if (!m_pedtPassport->hasAcceptableInput()) return false;
    input = m_pedtLName->text();
    if (m_nameValidator.validate(input, pos) != QValidator::Acceptable) return false;
    input = m_pedtFName->text();
    if (m_nameValidator.validate(input, pos) != QValidator::Acceptable) return false;
    input = m_pedtSName->text();
    if (m_nameValidator.validate(input, pos) != QValidator::Acceptable
        && !input.isEmpty()) return false;
    if (!m_pedtPhone->hasAcceptableInput()) return false;
    if (m_ptxtAddress->toPlainText() == "") return false;
    input = m_pedtEmail->text();
    if (m_emailValidator.validate(input, pos) != QValidator::Acceptable
        && !input.isEmpty()) return false;

    return true;
}
