#ifndef USERSEDITDIALOG_H
#define USERSEDITDIALOG_H

#include "UsersBaseDialog.h"

// Класс диалога изменения пользователя
// Предназначен для изменения данных в БД

class UsersEditDialog : public UsersBaseDialog {
    Q_OBJECT
private:
    QPushButton *m_pbtnSave;                    // Кнопки диалога
    QPushButton *m_pbtnCancel;

    QHBoxLayout *m_pbtnLayout;                  // Компоновки диалога
    QVBoxLayout *m_playout;

public:
    UsersEditDialog(QWidget *parent = nullptr);

public slots:
    virtual void accept();
};

#endif // USERSEDITDIALOG_H
