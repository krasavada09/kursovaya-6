#include "EmailValidator.h"

EmailValidator::EmailValidator(QObject *parent)
    : QRegExpValidator(parent) {
    setRegExp(QRegExp("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$"));
}
