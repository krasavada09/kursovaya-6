#include "AuthDialog.h"

AuthDialog::AuthDialog(QWidget *parent)
    : QDialog(parent,
              Qt::CustomizeWindowHint |
              Qt::WindowTitleHint |
              Qt::WindowCloseButtonHint
) {
    setModal(true);
    setWindowTitle("Авторизация");

    m_pedtLogin = new QLineEdit;
    m_pedtPassword = new QLineEdit;
    m_pedtPassword->setEchoMode(QLineEdit::Password);

    m_plblLogin = new QLabel("&Логин");
    m_plblPassword = new QLabel("&Пароль");

    m_plblLogin->setBuddy(m_pedtLogin);
    m_plblPassword->setBuddy(m_pedtPassword);

    m_pbtnEnter = new QPushButton("&Вход");
    m_pbtnCancel = new QPushButton("&Отмена");
    m_pbtnReg = new QPushButton("&Регистрация");
    m_pbtnEnter->setDefault(true);

    m_pregDialog = new RegDialog(this);

    // Соединение сигналов кнопок со слотами диалога
    connect(m_pbtnEnter, SIGNAL(clicked()), SLOT(accept()));
    connect(m_pbtnCancel, SIGNAL(clicked()), SLOT(reject()));
    connect(m_pbtnReg, SIGNAL(clicked()),
            m_pregDialog, SLOT(exec())
            );

    // Настройка компоновки
    m_pfrmLayout = new QFormLayout;
    m_pfrmLayout->addRow(m_plblLogin, m_pedtLogin);
    m_pfrmLayout->addRow(m_plblPassword, m_pedtPassword);

    m_pbtnLayout = new QHBoxLayout;
    m_pbtnLayout->addStretch();
    m_pbtnLayout->addWidget(m_pbtnEnter);
    m_pbtnLayout->addWidget(m_pbtnCancel);
    m_pbtnLayout->addStretch();

    m_playout = new QVBoxLayout;
    m_playout->addLayout(m_pfrmLayout);
    m_playout->addWidget(m_pbtnReg);
    m_playout->addLayout(m_pbtnLayout);

    setLayout(m_playout);
}

void AuthDialog::clear() {
// Возвращает форму в начальное состояние
    m_pedtLogin->setText("");
    m_pedtPassword->setText("");

    m_pedtLogin->setFocus();
}

int AuthDialog::exec() {
// Исполнение диалога
    clear();

    return QDialog::exec();
}

void AuthDialog::accept() {
// Принять диалог
    if (!formIsValid()) {
        QMessageBox::critical(this,
                              "Ошибка ввода",
                              "Заполните все поля",
                              QMessageBox::Ok
                              );
        return;
    }

    QByteArray arr = m_pedtPassword->text().toUtf8();
    arr = QCryptographicHash::hash(arr, QCryptographicHash::Sha256);
    QString password = arr.toHex();

    QSqlQuery query;
    query.prepare(
                "SELECT "
                "u.id, u.last_name, u.first_name, u.second_name, "
                "u.login, g.r, g.w, g.x "
                "FROM Users u "
                "LEFT JOIN Groups g ON (u.group_id = g.id) "
                "WHERE u.login = :login "
                "AND u.password = :password;"
                );
    query.bindValue(":login", m_pedtLogin->text());
    query.bindValue(":password", password);

    if (!query.exec()) {
        QMessageBox::critical(this,
                              "Ошибка авторизации",
                              "Не удалось выполнить авторизацию",
                              QMessageBox::Ok
                              );
        emit logout();
        return;
    }
    if (!query.first()) {
        QMessageBox::critical(this,
                              "Ошибка авторизации",
                              "Неверная пара логин-пароль",
                              QMessageBox::Ok
                              );
        emit logout();
        return;
    }

    User *user = new User(
              query.value("id").toInt(),
              query.value("last_name").toString(),
              query.value("first_name").toString(),
              query.value("second_name").toString(),
              query.value("login").toString(),
              query.value("r").toBool(),
              query.value("w").toBool(),
              query.value("x").toBool()
              );
    emit login(user);

    QDialog::accept();
}

bool AuthDialog::formIsValid() {
// Возвращает true, если все поля формы валидны
// Возвращает false иначе
    if (m_pedtLogin->text() == "") return false;
    if (m_pedtPassword->text() == "") return false;

    return true;
}
