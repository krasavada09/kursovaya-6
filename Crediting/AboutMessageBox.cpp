#include "AboutMessageBox.h"

AboutMessageBox::AboutMessageBox(QPixmap pixmap,
                                 const QString &title,
                                 const QString &text,
                                 QMessageBox::StandardButtons buttons,
                                 QWidget *parent,
                                 Qt::WindowFlags f)
    : QMessageBox(QMessageBox::NoIcon, title, text, buttons, parent, f) {
    setIconPixmap(pixmap);
}
