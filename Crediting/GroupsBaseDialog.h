#ifndef GROUPSBASEDIALOG_H
#define GROUPSBASEDIALOG_H

#include "DbDialog.h"

// Класс базового диалога группы
// Определяет поля, методы открытия формы

class GroupsBaseDialog : public DbDialog {
    Q_OBJECT
protected:
    int m_id;                                      // ID записи

    QLineEdit *m_pedtName;                         // Поля диалога
    QCheckBox *m_pchkR;
    QCheckBox *m_pchkW;
    QCheckBox *m_pchkX;

    QLabel *m_plblName;                            // Подписи диалога
    QLabel *m_plblR;
    QLabel *m_plblW;
    QLabel *m_plblX;

    QFormLayout *m_pfrmLayout;                     // Компоновка полей

public:
    GroupsBaseDialog(QWidget *parent = nullptr);

    void clear();
    bool formIsValid();

public slots:
    virtual int exec();
    virtual int exec(int &id);
};

#endif // GROUPSBASEDIALOG_H
