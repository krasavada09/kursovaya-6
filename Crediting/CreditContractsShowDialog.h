#ifndef CREDITCONTRACTSSHOWDIALOG_H
#define CREDITCONTRACTSSHOWDIALOG_H

#include "CreditContractsBaseDialog.h"

// Класс диалога просмотра договора
// Предназначен для вывода информации из БД

class CreditContractsShowDialog : public CreditContractsBaseDialog {
    Q_OBJECT
private:
    QPushButton *m_pbtnOk;                      // Кнопка диалога

    QHBoxLayout *m_pbtnLayout;                  // Компоновки диалога
    QVBoxLayout *m_playout;

public:
    CreditContractsShowDialog(QWidget *parent = nullptr);
};

#endif // CREDITCONTRACTSSHOWDIALOG_H
