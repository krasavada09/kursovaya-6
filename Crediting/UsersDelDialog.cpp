#include "UsersDelDialog.h"

UsersDelDialog::UsersDelDialog(QWidget *parent)
    : DbDialog(parent,
               Qt::CustomizeWindowHint |
               Qt::WindowTitleHint |
               Qt::WindowCloseButtonHint) {
    setWindowTitle("Удаление пользователя");

    m_plbl = new QLabel("Вы действительно хотите удалить запись?");

    m_pbtnYes = new QPushButton("&Да");
    m_pbtnNo = new QPushButton("&Нет");
    m_pbtnNo->setDefault(true);

    // Слот reject() по клику на кнопку "Нет"
    connect(m_pbtnNo, SIGNAL(clicked()),
            this, SLOT(reject())
            );
    // Слот accept() по клику на кнопку "Да"
    connect(m_pbtnYes, SIGNAL(clicked()),
            this, SLOT(accept())
            );

    // Настройка компоновки
    m_pbtnLayout = new QHBoxLayout;
    m_pbtnLayout->addStretch();
    m_pbtnLayout->addWidget(m_pbtnYes);
    m_pbtnLayout->addWidget(m_pbtnNo);
    m_pbtnLayout->addStretch();

    m_playout = new QVBoxLayout;
    m_playout->addWidget(m_plbl);
    m_playout->addLayout(m_pbtnLayout);

    setLayout(m_playout);
}

int UsersDelDialog::exec(int &id) {
// Исполнение диалога
    // Валидация id модели
    if (!id) return QDialog::Rejected;
    m_id = id;

    return QDialog::exec();
}

void UsersDelDialog::accept() {
// Принять диалог
    QSqlQuery query;
    query.prepare(
                "DELETE FROM Users "
                "WHERE id = :id;"
                );
    query.bindValue(":id", m_id);
    if (!query.exec()) {
        QMessageBox::critical(this,
                              "Ошибка удаления",
                              "Не удалось удалить пользователя",
                              QMessageBox::Ok
                              );
        return;
    }

    QDialog::accept();
}
