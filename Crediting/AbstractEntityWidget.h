#ifndef ABSTRACTENTITYWIDGET_H
#define ABSTRACTENTITYWIDGET_H

#include "DbDialog.h"
#include "EntityFindDialog.h"
#include "StyledSortFilterProxyModel.h"

// Абстрактный класс виджета
// Реализует функционал виджетов сущностей

class AbstractEntityWidget : public QWidget {
    Q_OBJECT
protected:
    QTableView *m_ptblView;                         // Представление сущности
    QSqlQueryModel *m_pqueryModel;                  // Модель сущности
    StyledSortFilterProxyModel *m_pproxyModel;      // Прокси-модель

    QModelIndex m_index;                            // Индекс модели
    int m_id;                                       // ID элемента
    QMenu *m_pmenu;                                 // Контекстное меню

    DbDialog *m_pshowDialog;                        // Диалоговые окна сущности
    DbDialog *m_paddDialog;
    DbDialog *m_peditDialog;
    DbDialog *m_pdelDialog;
    EntityFindDialog *m_pfindDialog;

    QPushButton *m_pbtnShow;                        // Кнопки формы
    QPushButton *m_pbtnAdd;
    QPushButton *m_pbtnEdit;
    QPushButton *m_pbtnDelete;

    QPushButton *m_pbtnFind;

    QPushButton *m_pbtnFirst;
    QPushButton *m_pbtnPrevious;
    QPushButton *m_pbtnNext;
    QPushButton *m_pbtnLast;

public:
    AbstractEntityWidget(QWidget *parent = nullptr);

    QSqlQueryModel* model();
    QTableView* view();

    void setShowDialog(DbDialog *dialog);
    void setAddDialog(DbDialog *dialog);
    void setEditDialog(DbDialog *dialog);
    void setDelDialog(DbDialog *dialog);
    void setFindDialog(EntityFindDialog *dialog);
    virtual void setContextMenu();

public slots:
    void setIndex(QModelIndex index);
    void refresh();
    void resize();
    void showContextMenu(const QPoint &pos);
};

#endif // ABSTRACTENTITYWIDGET_H
