#ifndef CUSTOMERSBASEDIALOG_H
#define CUSTOMERSBASEDIALOG_H

#include "DbDialog.h"
#include "NameValidator.h"
#include "EmailValidator.h"

// Класс базового диалога клиента
// Определяет поля, методы открытия формы

class CustomersBaseDialog : public DbDialog {
    Q_OBJECT
protected:
    int m_id;                                      // ID записи

    QLineEdit *m_pedtPassport;                     // Поля диалога
    QLineEdit *m_pedtLName;
    QLineEdit *m_pedtFName;
    QLineEdit *m_pedtSName;
    QComboBox *m_pcbbSex;
    QDateEdit *m_pdteBDate;
    QLineEdit *m_pedtPhone;
    QTextEdit *m_ptxtAddress;
    QLineEdit *m_pedtEmail;
    QComboBox *m_pcbbRatingID;

    NameValidator m_nameValidator;                 // Валидаторы данных
    EmailValidator m_emailValidator;

    QLabel *m_plblPassport;                        // Подписи диалога
    QLabel *m_plblLName;
    QLabel *m_plblFName;
    QLabel *m_plblSName;
    QLabel *m_plblSex;
    QLabel *m_plblBDate;
    QLabel *m_plblPhone;
    QLabel *m_plblAddress;
    QLabel *m_plblEmail;
    QLabel *m_plblRatingID;

    QFormLayout *m_pfrmLayout;                     // Компоновка полей

public:
    CustomersBaseDialog(QWidget *parent = nullptr);

    void clear();
    bool formIsValid();

public slots:
    virtual int exec();
    virtual int exec(int &id);
};

#endif // CUSTOMERSBASEDIALOG_H
