#ifndef GROUPSEDITDIALOG_H
#define GROUPSEDITDIALOG_H

#include "GroupsBaseDialog.h"

// Класс диалога изменения группы
// Предназначен для изменения данных в БД

class GroupsEditDialog : public GroupsBaseDialog {
    Q_OBJECT
private:
    QPushButton *m_pbtnSave;                    // Кнопки диалога
    QPushButton *m_pbtnCancel;

    QHBoxLayout *m_pbtnLayout;                  // Компоновки диалога
    QVBoxLayout *m_playout;

public:
    GroupsEditDialog(QWidget *parent = nullptr);

public slots:
    virtual void accept();
};

#endif // GROUPSEDITDIALOG_H
