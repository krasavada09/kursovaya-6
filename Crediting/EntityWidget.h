#ifndef ENTITYWIDGET_H
#define ENTITYWIDGET_H

#include "AbstractEntityWidget.h"

// Класс виджета сущности
// Определяет визуальное представление виджетов

class EntityWidget : public AbstractEntityWidget {
    Q_OBJECT
private:
    QHBoxLayout *m_pbtnLayout;                      // Компоновки виджета
    QVBoxLayout *m_playout;

public:
    EntityWidget(QWidget *parent = nullptr);
};

#endif // ENTITYWIDGET_H
