#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtWidgets>
#include <QtSql>

#include "User.h"
#include "AbstractEntityWidget.h"
#include "EntityWidget.h"
#include "PaymentSchedulesWidget.h"
#include "PaymentsWidget.h"
#include "CustomersShowDialog.h"
#include "CustomersAddDialog.h"
#include "CustomersEditDialog.h"
#include "CustomersDelDialog.h"
#include "CreditContractsShowDialog.h"
#include "CreditContractsAddDialog.h"
#include "CreditContractsEditDialog.h"
#include "CreditContractsDelDialog.h"
#include "SchedulesAddDialog.h"
#include "UsersShowDialog.h"
#include "UsersAddDialog.h"
#include "UsersEditDialog.h"
#include "UsersDelDialog.h"
#include "GroupsShowDialog.h"
#include "GroupsAddDialog.h"
#include "GroupsEditDialog.h"
#include "GroupsDelDialog.h"
#include "ExportDialog.h"
#include "CreditsReportDialog.h"
#include "DebtorsReport.h"
#include "ProfitReportDialog.h"
#include "PaymentScheduleReportDialog.h"
#include "AboutMessageBox.h"

class MainWindow : public QMainWindow {
    Q_OBJECT
private:
    QWidget *m_pwgt;

    QVector<QString> *m_ptableNames;                    // Имена таблиц
    QVector<AbstractEntityWidget *> *m_pentities;       // Сущности

    User *m_pUser;                                      // Пользователь программы

    ExportDialog *m_pexportDialog;                      // Диалоги программы
    CreditsReportDialog *m_pcreditRepDialog;
    DebtorsReport *m_pdebtorRep;
    ProfitReportDialog *m_pprofitRepDialog;
    PaymentScheduleReportDialog *m_ppaymentSchRepDialog;

    AboutMessageBox *m_paboutBox;

    QMenu *m_pmnuTable;                                 // Меню программы
    QMenu *m_pmnuReport;
    QMenu *m_pmnuUser;
    QMenu *m_pmnuAbout;

    QStackedLayout *m_playout;                          // Компоновка программы

public:
    MainWindow(User *user, QWidget *parent = nullptr);

    void setTables();
    void setMenu();
    void setEntities();
    void setLayout();

public slots:
    void show();

signals:
    void logouted();
};
#endif // MAINWINDOW_H
