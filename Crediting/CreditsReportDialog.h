#ifndef CREDITSREPORTDIALOG_H
#define CREDITSREPORTDIALOG_H

#include <QtWidgets>
#include <QtSql>
#include <QAxBase>
#include <QAxObject>
#include "User.h"
#include "AxFuntions.h"

// Диалог отчета "Выданные кредиты"
// Предназначен для формирования отчета

class CreditsReportDialog : public QDialog {
    Q_OBJECT
private:
    User *m_pUser;                              // Пользователь

    QDateEdit *m_pdteFirstDate;                 // Поля диалога
    QDateEdit *m_pdteLastDate;
    QLineEdit *m_pedtMinAmount;
    QLineEdit *m_pedtMaxAmount;

    QDoubleValidator m_dblValidator;            // Валидаторы данных

    QLabel *m_plblDate;                         // Подписи диалога
    QLabel *m_plblFirstDate;
    QLabel *m_plblLastDate;
    QLabel *m_plblAmount;
    QLabel *m_plblMinAmount;
    QLabel *m_plblMaxAmount;

    QPushButton *m_pbtnReport;                  // Кнопки диалога
    QPushButton *m_pbtnCancel;

    QHBoxLayout *m_pvBoxLayout1;                // Компоновки диалога
    QHBoxLayout *m_pvBoxLayout2;
    QHBoxLayout *m_pbtnLayout;
    QVBoxLayout *m_playout;

public:
    CreditsReportDialog(User *user, QWidget *parent = nullptr);

    void clear();
    bool formIsValid();

public slots:
    virtual int exec();
    virtual void accept();
};

#endif // CREDITSREPORTDIALOG_H
