#include "AxFuntions.h"

void writeSheet(QAxObject *sheet, QSqlQueryModel *model, QString tableName) {
// Заполняет лист Excel
// Принимает лист, модель сущности, название таблицы
    QModelIndex index;

    QAxObject *columns,
              *range,
              *cell;

    sheet->dynamicCall("SetName(const QString &)", tableName);

    // Заполнение шапки
    for (int i = 1; i < model->columnCount(); i++) {
        cell = sheet->querySubObject("Cells(int, int)", 1, i);
        cell->dynamicCall("SetValue(const QString &)",
                          model->headerData(i, Qt::Horizontal).toString()
                          );
    }

    // Заполнение тела таблицы
    for (int i = 0; i < model->rowCount(); ++i) {
        for (int j = 1; j < model->columnCount(); j++) {
            index = model->index(i, j);

            cell = sheet->querySubObject("Cells(int, int)", i+2, j);
            cell->dynamicCall("SetValue(const QVariant &)",
                              index.data().isNull() ?
                                  QVariant(QVariant::String) :
                                  index.data()
                             );
        }
    }

    // Стили шапки таблицы
    range = sheet->querySubObject(
                "Range(QVariant, QVariant)",
                sheet->querySubObject("Cells(int, int)", 1, 1)->asVariant(),
                sheet->querySubObject(
                    "Cells(int, int)",
                    1, model->columnCount()-1)->asVariant()
                );
    range->querySubObject("Font")->dynamicCall("Bold", true);
    range->querySubObject("Font")
            ->dynamicCall("SetColor(QColor)", QColor(255, 255, 255));
    range->querySubObject("Interior")
            ->dynamicCall("SetColor(QColor)", QColor(112, 173, 71));
    range->dynamicCall("SetHorizontalAlignment(int)", 7);

    // Стили тела таблицы
    range = sheet->querySubObject(
                "Range(const QVariant &, const QVariant &)",
                sheet->querySubObject("Cells(int, int)", 2, 1)->asVariant(),
                sheet->querySubObject(
                    "Cells(int, int)",
                    model->rowCount()+1, model->columnCount()-1)->asVariant()
                );
    range->querySubObject("Interior")
            ->dynamicCall("SetColor(QColor)", QColor(226, 239, 218));

    // Границы таблицы
    for (int i = 1; i < model->rowCount()+2; i++) {
        for (int j = 1; j < model->columnCount(); j++) {
            cell = sheet->querySubObject("Cells(int, int)", i, j);
            cell->dynamicCall("BorderAround(int, int", 1, 2);
        }
    }

    // Выравнивание ширины колонок
    columns = sheet->querySubObject("Columns");
    columns->dynamicCall("AutoFit()");
}
