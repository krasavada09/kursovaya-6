#include "GroupsEditDialog.h"

GroupsEditDialog::GroupsEditDialog(QWidget *parent)
    :GroupsBaseDialog(parent) {
    setWindowTitle("Редактирование группы");

    m_pbtnSave = new QPushButton("&Сохранить");
    m_pbtnCancel = new QPushButton("&Отмена");
    m_pbtnSave->setDefault(true);

    // Слот reject() по клику на кнопку "Отмена"
    connect(m_pbtnCancel, SIGNAL(clicked()),
            this, SLOT(reject())
            );
    // Слот accept() по клику на кнопку "Сохранить"
    connect(m_pbtnSave, SIGNAL(clicked()),
            this, SLOT(accept())
            );

    // Настройка компоновки
    m_pbtnLayout = new QHBoxLayout;
    m_pbtnLayout->addStretch();
    m_pbtnLayout->addWidget(m_pbtnSave);
    m_pbtnLayout->addWidget(m_pbtnCancel);
    m_pbtnLayout->addStretch();

    m_playout = new QVBoxLayout;
    m_playout->addLayout(m_pfrmLayout);
    m_playout->addLayout(m_pbtnLayout);

    setLayout(m_playout);
}

void GroupsEditDialog::accept() {
// Принять диалог
    if (!formIsValid()) {
        QMessageBox::critical(this,
                              "Ошибка ввода",
                              "Проверьте правильность введенных данных",
                              QMessageBox::Ok
                              );
        return;
    }

    QSqlQuery query;
    query.prepare(
                "UPDATE Groups "
                "SET "
                "name = :name, r = :r, w = :w, x = :x "
                "WHERE id = :id;"
                );
    query.bindValue(":name", m_pedtName->text());
    query.bindValue(":r", m_pchkR->isChecked());
    query.bindValue(":w", m_pchkW->isChecked());
    query.bindValue(":x", m_pchkX->isChecked());
    query.bindValue(":id", m_id);

    // Запрос на обновление группы
    if (!query.exec()) {
        QMessageBox::critical(this,
                              "Ошибка редактирования",
                              "Не удалось сохранить группу",
                              QMessageBox::Ok
                              );
        return;
    }

    QDialog::accept();
}
