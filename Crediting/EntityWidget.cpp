#include "EntityWidget.h"

EntityWidget::EntityWidget(QWidget *parent)
    : AbstractEntityWidget(parent) {
    // Настройка компоновки кнопок
    m_pbtnLayout = new QHBoxLayout;
    m_pbtnLayout->addWidget(m_pbtnFirst);
    m_pbtnLayout->addWidget(m_pbtnPrevious);
    m_pbtnLayout->addWidget(m_pbtnNext);
    m_pbtnLayout->addWidget(m_pbtnLast);
    m_pbtnLayout->addWidget(m_pbtnShow);
    m_pbtnLayout->addWidget(m_pbtnAdd);
    m_pbtnLayout->addWidget(m_pbtnEdit);
    m_pbtnLayout->addWidget(m_pbtnDelete);
    m_pbtnLayout->addWidget(m_pbtnFind);
    m_pbtnLayout->addStretch();

    // Настройка компоновки виджета
    m_playout = new QVBoxLayout;
    m_playout->addLayout(m_pbtnLayout);
    m_playout->addWidget(m_ptblView);

    setLayout(m_playout);
}
