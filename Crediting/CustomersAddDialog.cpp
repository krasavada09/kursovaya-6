#include "CustomersAddDialog.h"

CustomersAddDialog::CustomersAddDialog(QWidget *parent)
    : CustomersBaseDialog(parent) {
    setWindowTitle("Добавление клиента");

    m_pbtnAdd = new QPushButton("&Добавить");
    m_pbtnCancel = new QPushButton("&Отмена");
    m_pbtnAdd->setDefault(true);

    // Слот reject() по клику на кнопку "Отмена"
    connect(m_pbtnCancel, SIGNAL(clicked()),
            this, SLOT(reject())
            );
    // Слот accept() по клику на кнопку "Добавить"
    connect(m_pbtnAdd, SIGNAL(clicked()),
            this, SLOT(accept())
            );

    // Настройка компоновки
    m_pbtnLayout = new QHBoxLayout;
    m_pbtnLayout->addStretch();
    m_pbtnLayout->addWidget(m_pbtnAdd);
    m_pbtnLayout->addWidget(m_pbtnCancel);
    m_pbtnLayout->addStretch();

    m_playout = new QVBoxLayout;
    m_playout->addLayout(m_pfrmLayout);
    m_playout->addLayout(m_pbtnLayout);

    setLayout(m_playout);
}

void CustomersAddDialog::accept() {
// Принять диалог
    if (!formIsValid()) {
        QMessageBox::critical(this,
                              "Ошибка ввода",
                              "Проверьте правильность введенных данных",
                              QMessageBox::Ok
                              );
        return;
    }

    QSqlQuery query;
    query.prepare(
                "INSERT INTO Customers "
                "(passport, last_name, first_name, second_name, "
                "sex, birth_date, phone, address, email, credit_rating_id) "
                "VALUES "
                "(:passport, :last_name, :first_name, :second_name, "
                ":sex, :birth_date, :phone, :address, :email, :credit_rating_id);"
                );
    query.bindValue(":passport", m_pedtPassport->text());
    query.bindValue(":last_name", m_pedtLName->text());
    query.bindValue(":first_name", m_pedtFName->text());
    query.bindValue(":second_name", m_pedtSName->text());
    query.bindValue(":sex", m_pcbbSex->currentData().toString());
    query.bindValue(":birth_date", m_pdteBDate->date().toString("yyyy-MM-dd"));
    query.bindValue(":phone", m_pedtPhone->text());
    query.bindValue(":address", m_ptxtAddress->toPlainText());
    query.bindValue(":email", m_pedtEmail->text());
    query.bindValue(":credit_rating_id",
                    m_pcbbRatingID->currentData().toString().isEmpty() ?
                        QVariant(QVariant::String) :
                        m_pcbbRatingID->currentData().toString()
                    );

    // Запрос на добавление клиента
    if (!query.exec()) {
        QMessageBox::critical(this,
                              "Ошибка добавления",
                              "Не удалось добавить клиента",
                              QMessageBox::Ok
                              );
        return;
    }

    QDialog::accept();
}
