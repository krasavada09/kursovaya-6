#include "CreditsReportDialog.h"

CreditsReportDialog::CreditsReportDialog(
        User *user,
        QWidget *parent)
    : QDialog(parent,
              Qt::CustomizeWindowHint |
              Qt::WindowTitleHint |
              Qt::WindowCloseButtonHint |
              Qt::WindowMaximizeButtonHint
            ), m_pUser(user) {
    setWindowTitle("Сформировать отчет");

    m_pdteFirstDate = new QDateEdit;
    m_pdteLastDate = new QDateEdit;
    m_pedtMinAmount = new QLineEdit;
    m_pedtMaxAmount = new QLineEdit;

    m_pdteFirstDate->setMaximumWidth(150);
    m_pdteLastDate->setMaximumWidth(150);
    m_pedtMinAmount->setMaximumWidth(200);
    m_pedtMaxAmount->setMaximumWidth(200);

    m_pdteFirstDate->setCalendarPopup(true);
    m_pdteLastDate->setCalendarPopup(true);

    m_pedtMinAmount->setValidator(&m_dblValidator);
    m_pedtMaxAmount->setValidator(&m_dblValidator);

    m_plblDate = new QLabel("Дата выдачи");
    m_plblFirstDate = new QLabel("c *:");
    m_plblLastDate = new QLabel("по *:");
    m_plblAmount = new QLabel("Сумма кредита");
    m_plblMinAmount = new QLabel("от *:");
    m_plblMaxAmount = new QLabel("до *:");

    m_pbtnReport = new QPushButton("&Формировать");
    m_pbtnCancel = new QPushButton("&Отмена");
    m_pbtnReport->setDefault(true);

    // Слот reject() по клику на кнопку "Отмена"
    connect(m_pbtnCancel, SIGNAL(clicked()),
            this, SLOT(reject())
            );
    // Слот accept() по клику на кнопку "Формировать"
    connect(m_pbtnReport, SIGNAL(clicked()),
            this, SLOT(accept())
            );

    // Настройка компоновки
    m_pvBoxLayout1 = new QHBoxLayout;
    m_pvBoxLayout1->addWidget(m_plblFirstDate);
    m_pvBoxLayout1->addSpacing(10);
    m_pvBoxLayout1->addWidget(m_pdteFirstDate);
    m_pvBoxLayout1->addSpacing(20);
    m_pvBoxLayout1->addWidget(m_plblLastDate);
    m_pvBoxLayout1->addSpacing(10);
    m_pvBoxLayout1->addWidget(m_pdteLastDate);
    m_pvBoxLayout1->addStretch();

    m_pvBoxLayout2 = new QHBoxLayout;
    m_pvBoxLayout2->addWidget(m_plblMinAmount);
    m_pvBoxLayout2->addSpacing(10);
    m_pvBoxLayout2->addWidget(m_pedtMinAmount);
    m_pvBoxLayout2->addSpacing(20);
    m_pvBoxLayout2->addWidget(m_plblMaxAmount);
    m_pvBoxLayout2->addSpacing(10);
    m_pvBoxLayout2->addWidget(m_pedtMaxAmount);
    m_pvBoxLayout2->addStretch();

    m_pbtnLayout = new QHBoxLayout;
    m_pbtnLayout->addStretch();
    m_pbtnLayout->addWidget(m_pbtnReport);
    m_pbtnLayout->addWidget(m_pbtnCancel);
    m_pbtnLayout->addStretch();

    m_playout = new QVBoxLayout;
    m_playout->addWidget(m_plblDate);
    m_playout->addItem(m_pvBoxLayout1);
    m_playout->addWidget(m_plblAmount);
    m_playout->addItem(m_pvBoxLayout2);
    m_playout->addItem(m_pbtnLayout);

    setLayout(m_playout);
}

void CreditsReportDialog::clear() {
// Возвращает форму в начальное состояние
    m_pdteFirstDate->setDate(QDate::currentDate());
    m_pdteLastDate->setDate(QDate::currentDate());
    m_pedtMinAmount->setText("");
    m_pedtMaxAmount->setText("");

    m_pdteFirstDate->setFocus();
}

bool CreditsReportDialog::formIsValid() {
// Возвращает true, если все поля формы валидны
// Возвращает false иначе
    QString input; int pos = 0;

    input = m_pedtMinAmount->text();
    if (m_dblValidator.validate(input, pos) != QValidator::Acceptable
            && !input.isEmpty()) return false;
    input = m_pedtMaxAmount->text();
    if (m_dblValidator.validate(input, pos) != QValidator::Acceptable) return false;

    return true;
}

int CreditsReportDialog::exec() {
// Исполнение диалога
    clear();

    return QDialog::exec();
}

void CreditsReportDialog::accept() {
// Принять диалог
    if (!formIsValid()) {
        QMessageBox::critical(this,
                              "Ошибка ввода",
                              "Проверьте правильность введенных данных",
                              QMessageBox::Ok
                              );
        return;
    }

    QSqlQueryModel model;
    QString queryString;
    queryString = "SELECT "
                  "cc.id, "
                  "cc.id AS №, "
                  "u.last_name || ' ' || "
                  "u.first_name || ' ' || "
                  "u.second_name AS Сотрудник, "
                  "pr.name AS Обеспечение, pp.name AS Цель, "
                  "rm.name AS \"Метод погашения\", f.name AS \"Форма выдачи\", "
                  "c.last_name || ' ' || "
                  "c.first_name || ' ' || "
                  "c.second_name AS Клиент, "
                  "cc.amount AS Сумма, cc.rate AS Ставка, cc.term AS Срок, "
                  "cc.date AS \"Дата выдачи\", "
                  "cc.insurance_contract_id AS \"Договор страхования\" "
                  "FROM Credit_Contracts cc "
                  "JOIN Users u ON cc.user_id = u.id "
                  "JOIN Provisions pr ON cc.provision_id = pr.id "
                  "JOIN Purposes pp ON cc.purpose_id = pp.id "
                  "JOIN Repayment_Methods rm ON cc.repayment_method_id = rm.id "
                  "JOIN Forms f ON cc.form_id = f.id "
                  "JOIN Customers c ON cc.customer_id = c.id ";
    if (!m_pUser->w())
        queryString += "WHERE u.id = '" + QString::number(m_pUser->id()) + "' ";
    else queryString += "WHERE true ";
    queryString += "AND (cc.date BETWEEN '" +
            m_pdteFirstDate->date().toString("yyyy-MM-dd") +
            "' AND '" +
            m_pdteLastDate->date().toString("yyyy-MM-dd") + "') ";
    QLocale locale(QLocale::Russian);
    queryString += "AND (cc.amount BETWEEN '" +
            QString::number(locale.toDouble(m_pedtMinAmount->text())) +
            "' AND '" +
            QString::number(locale.toDouble(m_pedtMaxAmount->text())) + "') ";
    queryString += "ORDER BY cc.id;";

    model.setQuery(queryString);
    if (model.lastError().type() != QSqlError::NoError) {
        QMessageBox::critical(this,
                              "Ошибка чтения",
                              "Не удалось считать данные договора",
                              QMessageBox::Ok
                              );
        return;
    }
    if (model.rowCount() < 1) {
        QMessageBox::information(this,
                              "Записи не найдены",
                              "Нет записей, удовлетворяющих условиям",
                              QMessageBox::Ok
                              );
        return;
    }

    QAxObject *excel,
              *workbooks,
              *workbook,
              *sheets,
              *sheet;

    // Создание документа
    excel = new QAxObject("Excel.Application", 0);
    excel->dynamicCall("SetDisplayAlerts(bool)", false);
    excel->dynamicCall("SetVisible(bool)", false);
    workbooks = excel->querySubObject("Workbooks");
    workbooks->dynamicCall("Add()");
    workbook = workbooks->querySubObject("Item(int)", 1);
    sheets = workbook->querySubObject("Worksheets");
    sheet = sheets->querySubObject("Item(int)", 1);

    // Заполнение листа
    writeSheet(sheet, &model, "Выданные кредиты");

    excel->dynamicCall("SetDisplayAlerts(bool)", true);
    excel->dynamicCall("SetVisible(bool)", true);

    QDialog::accept();
}
