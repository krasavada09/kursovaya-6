#include "GroupsBaseDialog.h"

GroupsBaseDialog::GroupsBaseDialog(QWidget *parent)
    : DbDialog(parent,
                Qt::CustomizeWindowHint |
                Qt::WindowTitleHint |
                Qt::WindowCloseButtonHint |
                Qt::WindowMaximizeButtonHint) {
      setModal(true);

      m_pedtName = new QLineEdit;
      m_pchkR = new QCheckBox;
      m_pchkW = new QCheckBox;
      m_pchkX = new QCheckBox;

      m_pedtName->setMaxLength(30);

      m_plblName = new QLabel("Название *");
      m_plblR = new QLabel("Просмотр");
      m_plblW = new QLabel("Редактирование");
      m_plblX = new QLabel("Полный доступ");

      // Выбор всех чекбоксов нажатием на "Полный доступ"
      connect(m_pchkX, &QCheckBox::stateChanged,
              [this](int state) {
                  if (state == Qt::Checked) {
                      m_pchkR->setChecked(true);
                      m_pchkW->setChecked(true);
                  }
              }
              );

      // Настройка компоновки
      m_pfrmLayout = new QFormLayout;
      m_pfrmLayout->addRow(m_plblName, m_pedtName);
      m_pfrmLayout->addRow(m_plblR, m_pchkR);
      m_pfrmLayout->addRow(m_plblW, m_pchkW);
      m_pfrmLayout->addRow(m_plblX, m_pchkX);

      resize(350, height());
}

void GroupsBaseDialog::clear() {
// Возвращает форму в начальное состояние
    m_pedtName->setText("");
    m_pchkR->setChecked(false);
    m_pchkW->setChecked(false);
    m_pchkX->setChecked(false);

    m_pedtName->setFocus();
}

int GroupsBaseDialog::exec() {
// Исполнение диалога
    clear();

    return QDialog::exec();
}

int GroupsBaseDialog::exec(int &id) {
// Исполнение диалога
    clear();

    // Валидация id модели
    if (!id) return QDialog::Rejected;
    m_id = id;

    QSqlQuery query;
    query.prepare(
                "SELECT "
                "id, name, r, w, x "
                "FROM Groups "
                "WHERE id = :id;"
                );
    query.bindValue(":id", m_id);
    if (!query.exec() || query.size() > 1) {
        QMessageBox::critical(this,
                              "Ошибка чтения",
                              "Не удалось считать данные группы",
                              QMessageBox::Ok
                              );
        return QDialog::Rejected;
    }

    query.first();

    // Вывод данных в поля диалога
    m_pedtName->setText(query.value("name").toString());
    m_pchkR->setChecked(query.value("r").toBool());
    m_pchkW->setChecked(query.value("w").toBool());
    m_pchkX->setChecked(query.value("x").toBool());

    return QDialog::exec();
}

bool GroupsBaseDialog::formIsValid() {
// Возвращает true, если все поля формы валидны
// Возвращает false иначе
    if (m_pchkX->isChecked()) {
        m_pchkR->setChecked(true);
        m_pchkW->setChecked(true);
    }

    if (m_pedtName->text() == "") return false;

    return true;
}
