#ifndef ENTITYFINDDIALOG_H
#define ENTITYFINDDIALOG_H

#include "DbDialog.h"
#include "StyledSortFilterProxyModel.h"

// Класс диалога поиска
// Предназначен для поиска данных в таблице

class EntityFindDialog : public DbDialog {
    Q_OBJECT
private:
    StyledSortFilterProxyModel *m_pentityModel;  // Модель сущности

    QComboBox *m_pcbbField;                      // Поля диалога
    QLineEdit *m_pedtValue;
    QRadioButton *m_pradFilter;
    QRadioButton *m_pradMark;
    QCheckBox *m_pchkCaseSensivity;

    QLabel *m_plblField;                         // Подписи диалога
    QLabel *m_plblValue;

    QPushButton *m_pbtnFind;                     // Кнопки диалога
    QPushButton *m_pbtnCancel;

    QFormLayout *m_pfrmLayout;                   // Компоновки диалога
    QHBoxLayout *m_pbtnLayout;
    QVBoxLayout *m_playout;

public:
    EntityFindDialog(QWidget *parent = nullptr);

    void clear();

    void filter();
    void mark();
    void cancel();

public slots:
    virtual int exec();
    virtual int exec(int &);
    virtual int exec(StyledSortFilterProxyModel *model);

    virtual void accept();
};

#endif // ENTITYFINDDIALOG_H
