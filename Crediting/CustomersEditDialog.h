#ifndef CUSTOMERSEDITDIALOG_H
#define CUSTOMERSEDITDIALOG_H

#include "CustomersBaseDialog.h"

// Класс диалога изменения клиента
// Предназначен для изменения данных в БД

class CustomersEditDialog : public CustomersBaseDialog {
    Q_OBJECT
private:
    QPushButton *m_pbtnSave;                    // Кнопки диалога
    QPushButton *m_pbtnCancel;

    QHBoxLayout *m_pbtnLayout;                  // Компоновки диалога
    QVBoxLayout *m_playout;

public:
    CustomersEditDialog(QWidget *parent = nullptr);

public slots:
    virtual void accept();
};

#endif // CUSTOMERSEDITDIALOG_H
