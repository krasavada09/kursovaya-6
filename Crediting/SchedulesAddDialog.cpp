#include "SchedulesAddDialog.h"

SchedulesAddDialog::SchedulesAddDialog(QWidget *parent)
    : DbDialog(parent,
              Qt::CustomizeWindowHint |
              Qt::WindowTitleHint |
              Qt::WindowCloseButtonHint |
              Qt::WindowMaximizeButtonHint) {
    setModal(true);
    setWindowTitle("Добавление графика платежей");

    m_pedtContractID = new QLineEdit;
    m_plblContractID = new QLabel("Номер договора");

    m_pedtContractID->setValidator(&m_intValidator);

    m_pbtnAdd = new QPushButton("&Добавить");
    m_pbtnCancel = new QPushButton("&Отмена");
    m_pbtnAdd->setDefault(true);

    // Слот reject() по клику на кнопку "Отмена"
    connect(m_pbtnCancel, SIGNAL(clicked()),
            this, SLOT(reject())
            );
    // Слот accept() по клику на кнопку "Добавить"
    connect(m_pbtnAdd, SIGNAL(clicked()),
            this, SLOT(accept())
            );

    // Настройка компоновки
    m_pfrmLayout = new QFormLayout;
    m_pfrmLayout->addRow(m_plblContractID, m_pedtContractID);

    m_pbtnLayout = new QHBoxLayout;
    m_pbtnLayout->addStretch();
    m_pbtnLayout->addWidget(m_pbtnAdd);
    m_pbtnLayout->addWidget(m_pbtnCancel);
    m_pbtnLayout->addStretch();

    m_playout = new QVBoxLayout;
    m_playout->addLayout(m_pfrmLayout);
    m_playout->addLayout(m_pbtnLayout);

    setLayout(m_playout);

    resize(350, height());
}

void SchedulesAddDialog::clear() {
// Возвращает форму в начальное состояние
    m_pedtContractID->setText("");
    m_pedtContractID->setFocus();
}

int SchedulesAddDialog::exec() {
// Исполнение диалога
    clear();

    return QDialog::exec();
}

bool SchedulesAddDialog::formIsValid() {
// Возвращает true, если все поля формы валидны
// Возвращает false иначе
    QString input; int pos = 0;

    input = m_pedtContractID->text();
    if (m_intValidator.validate(input, pos) != QValidator::Acceptable) return false;

    return true;
}

void SchedulesAddDialog::accept() {
// Принять диалог
    if (!formIsValid()) {
        QMessageBox::critical(this,
                              "Ошибка ввода",
                              "Проверьте правильность введенных данных",
                              QMessageBox::Ok
                              );
        return;
    }

    QString id = m_pedtContractID->text();

    QSqlQuery query;
    // Выборка данных о договоре
    query.prepare("SELECT * FROM Credit_Contracts WHERE id = :id");
    query.bindValue(":id", id);
    if (!query.exec()) {
        QMessageBox::critical(this,
                              "Ошибка добавления",
                              "Не удалось сформировать график",
                              QMessageBox::Ok
                              );
        return;
    }
    if (!query.first()) {
        QMessageBox::critical(this,
                              "Ошибка ввода",
                              "Не договор с таким номером не найден",
                              QMessageBox::Ok
                              );
        return;
    }

    // Подсчет графика
    double amount = query.value("amount").toDouble();
    double rate = query.value("rate").toDouble() / 100;
    int term = query.value("term").toInt();
    double paymentAmount = getPaymentAmount(amount, rate, term);
    QDate date = query.value("date").toDate();

    QSqlDatabase::database().transaction();
    // Добавление графика платежей
    query.prepare(
                "INSERT INTO Payment_Schedules "
                "(credit_contract_id) "
                "VALUES "
                "(:id);"
                );
    query.bindValue(":id", id);
    if (!query.exec()) {
        QMessageBox::critical(this,
                              "Ошибка добавления",
                              "Не удалось сформировать график",
                              QMessageBox::Ok
                              );
        QSqlDatabase::database().rollback();
        return;
    }
    id = query.lastInsertId().toString();

    // Добавление пунктов графика
    QString queryText = "INSERT INTO Schedule_Items "
                        "(payment_schedule_id, date, amount) "
                        "VALUES ";
    QString value;
    for (int i = 0; i < term; i++, date = date.addMonths(1)) {
        value = QString("('%1', '%2', '%3')").arg(id)
                                             .arg(date.toString("yyyy-MM-dd"))
                                             .arg(paymentAmount);
        queryText += value;
        queryText += (i == term - 1) ? ";" : ",";
    }
    if (!query.exec(queryText)) {
        QMessageBox::critical(this,
                              "Ошибка добавления",
                              "Не удалось сформировать график",
                              QMessageBox::Ok
                              );
        QSqlDatabase::database().rollback();
        return;
    }

    QSqlDatabase::database().commit();

    QDialog::accept();
}
