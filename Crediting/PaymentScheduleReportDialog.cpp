#include "PaymentScheduleReportDialog.h"

PaymentScheduleReportDialog::PaymentScheduleReportDialog(QWidget *parent)
    : QDialog(parent,
              Qt::CustomizeWindowHint |
              Qt::WindowTitleHint |
              Qt::WindowCloseButtonHint |
              Qt::WindowMaximizeButtonHint
            ) {
    setWindowTitle("Сформировать отчет");

    m_pedtContractID = new QLineEdit;
    m_pedtContractID->setValidator(&m_intValidator);
    m_plblContractID = new QLabel("Номер договора");

    m_pbtnReport = new QPushButton("&Формировать");
    m_pbtnCancel = new QPushButton("&Отмена");
    m_pbtnReport->setDefault(true);

    // Слот reject() по клику на кнопку "Отмена"
    connect(m_pbtnCancel, SIGNAL(clicked()),
            this, SLOT(reject())
            );
    // Слот accept() по клику на кнопку "Формировать"
    connect(m_pbtnReport, SIGNAL(clicked()),
            this, SLOT(accept())
            );

    // Настройка компоновки
    m_pfrmLayout = new QFormLayout;
    m_pfrmLayout->addRow(m_plblContractID, m_pedtContractID);
    m_pfrmLayout->setSpacing(10);

    m_pbtnLayout = new QHBoxLayout;
    m_pbtnLayout->addStretch();
    m_pbtnLayout->addWidget(m_pbtnReport);
    m_pbtnLayout->addWidget(m_pbtnCancel);
    m_pbtnLayout->addStretch();

    m_playout = new QVBoxLayout;
    m_playout->addItem(m_pfrmLayout);
    m_playout->addItem(m_pbtnLayout);

    setLayout(m_playout);

    resize(300, height());
}

void PaymentScheduleReportDialog::clear() {
// Возвращает форму в начальное состояние
    m_pedtContractID->setText("");
    m_pedtContractID->setFocus();
}

int PaymentScheduleReportDialog::exec() {
// Исполнение диалога
    clear();

    return QDialog::exec();
}

bool PaymentScheduleReportDialog::formIsValid() {
// Возвращает true, если все поля формы валидны
// Возвращает false иначе
    QString input; int pos = 0;

    input = m_pedtContractID->text();
    if (m_intValidator.validate(input, pos) != QValidator::Acceptable) return false;

    return true;
}

void PaymentScheduleReportDialog::accept() {
// Принять диалог
    if (!formIsValid()) {
        QMessageBox::critical(this,
                              "Ошибка ввода",
                              "Проверьте правильность введенных данных",
                              QMessageBox::Ok
                              );
        return;
    }

    QSqlQueryModel model;
    QString queryString = "WITH Payment_Schedules_Current AS ( "
                          "SELECT "
                          "MAX(id) AS id, credit_contract_id "
                          "FROM Payment_Schedules "
                          "GROUP BY credit_contract_id "
                          ") "
                          "SELECT "
                          "si.id, "
                          "psc.credit_contract_id AS \"№ Договора\", si.date AS Дата, "
                          "si.amount AS Сумма "
                          "FROM Schedule_Items si "
                          "JOIN Payment_Schedules_Current psc "
                          "ON (psc.id = si.payment_schedule_id) "
                          "JOIN Credit_Contracts cc ON (cc.id = psc.credit_contract_id) "
                          "JOIN Users u ON (u.id = cc.user_id) "
                          "WHERE cc.id = '%1' "
                          "ORDER BY cc.id DESC, si.date;";
    queryString = queryString.arg(m_pedtContractID->text());

    model.setQuery(queryString);
    if (model.lastError().type() != QSqlError::NoError) {
        QMessageBox::critical(this,
                              "Ошибка чтения",
                              "Не удалось считать данные договоров",
                              QMessageBox::Ok
                              );
        return;
    }
    if (model.rowCount() < 1) {
        QMessageBox::information(this,
                              "Записи не найдены",
                              "Нет записей, удовлетворяющих условиям",
                              QMessageBox::Ok
                              );
        return;
    }

    QAxObject *excel,
              *workbooks,
              *workbook,
              *sheets,
              *sheet;

    // Создание документа
    excel = new QAxObject("Excel.Application", 0);
    excel->dynamicCall("SetDisplayAlerts(bool)", false);
    excel->dynamicCall("SetVisible(bool)", false);
    workbooks = excel->querySubObject("Workbooks");
    workbooks->dynamicCall("Add()");
    workbook = workbooks->querySubObject("Item(int)", 1);
    sheets = workbook->querySubObject("Worksheets");
    sheet = sheets->querySubObject("Item(int)", 1);

    // Заполнение листа
    writeSheet(sheet, &model, "График платежей");

    excel->dynamicCall("SetDisplayAlerts(bool)", true);
    excel->dynamicCall("SetVisible(bool)", true);

    QDialog::accept();
}
