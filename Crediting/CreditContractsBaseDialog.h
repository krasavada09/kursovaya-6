#ifndef CREDITCONTRACTSBASEDIALOG_H
#define CREDITCONTRACTSBASEDIALOG_H

#include "DbDialog.h"

// Класс базового диалога договора
// Определяет поля, методы открытия формы

class CreditContractsBaseDialog : public DbDialog {
    Q_OBJECT
protected:
    int m_id;                                      // ID записи
    int m_userID;                                  // ID пользователя

    QComboBox *m_pcbbProvisionID;                  // Поля диалога
    QComboBox *m_pcbbPurposeID;
    QComboBox *m_pcbbRepaymentMethodID;
    QComboBox *m_pcbbFormID;
    QComboBox *m_pcbbCustomerID;
    QLineEdit *m_pedtAmount;
    QLineEdit *m_pedtRate;
    QLineEdit *m_pedtTerm;
    QDateEdit *m_pdteDate;
    QLineEdit *m_pedtInsuranceContractID;

    QDoubleValidator m_dblValidator;              // Валидаторы данных
    QIntValidator m_intValidator;

    QLabel *m_plblProvisionID;                    // Подписи диалога
    QLabel *m_plblPurposeID;
    QLabel *m_plblRepaymentMethodID;
    QLabel *m_plblFormID;
    QLabel *m_plblCustomerID;
    QLabel *m_plblAmount;
    QLabel *m_plblRate;
    QLabel *m_plblTerm;
    QLabel *m_plblDate;
    QLabel *m_plblInsuranceContractID;

    QFormLayout *m_pfrmLayout;                    // Компоновка полей

public:
    CreditContractsBaseDialog(QWidget *parent = nullptr);

    void clear();
    bool formIsValid();

public slots:
    virtual int exec();
    virtual int exec(int &id);
};

#endif // CREDITCONTRACTSBASEDIALOG_H
