#ifndef USER_H
#define USER_H

#include <QString>

// Класс пользователя
// Реализует объект пользователя программы

class User {
private:
    int *m_pID;
    QString *m_plast_name;
    QString *m_pfirst_name;
    QString *m_psecond_name;
    QString *m_plogin;
    bool *m_pr;
    bool *m_pw;
    bool *m_px;

public:
    User();
    User(int id,
         QString last_name,
         QString first_name,
         QString second_name,
         QString login,
         bool r, bool w, bool x
         );
    ~User();

    void setUser(
                int id,
                QString last_name,
                QString first_name,
                QString second_name,
                QString login,
                bool r, bool w, bool x
                );

    int id();
    QString login();
    bool r();
    bool w();
    bool x();
};

#endif // USER_H
