#include "UsersAddDialog.h"

UsersAddDialog::UsersAddDialog(QWidget *parent)
    : UsersBaseDialog(parent) {
    setWindowTitle("Добавление пользователя");

    m_pbtnAdd = new QPushButton("&Добавить");
    m_pbtnCancel = new QPushButton("&Отмена");
    m_pbtnAdd->setDefault(true);

    // Слот reject() по клику на кнопку "Отмена"
    connect(m_pbtnCancel, SIGNAL(clicked()),
            this, SLOT(reject())
            );
    // Слот accept() по клику на кнопку "Добавить"
    connect(m_pbtnAdd, SIGNAL(clicked()),
            this, SLOT(accept())
            );

    // Настройка компоновки
    m_pbtnLayout = new QHBoxLayout;
    m_pbtnLayout->addStretch();
    m_pbtnLayout->addWidget(m_pbtnAdd);
    m_pbtnLayout->addWidget(m_pbtnCancel);
    m_pbtnLayout->addStretch();

    m_playout = new QVBoxLayout;
    m_playout->addLayout(m_pfrmLayout);
    m_playout->addLayout(m_pbtnLayout);

    setLayout(m_playout);
}

void UsersAddDialog::accept() {
// Принять диалог
    if (!formIsValid()) {
        QMessageBox::critical(this,
                              "Ошибка ввода",
                              "Проверьте правильность введенных данных",
                              QMessageBox::Ok
                              );
        return;
    }

    QByteArray arr = QString("").toUtf8();
    arr = QCryptographicHash::hash(arr, QCryptographicHash::Sha256);
    QString password = arr.toHex();

    QSqlQuery query;
    query.prepare(
                "INSERT INTO Users "
                "(group_id, last_name, first_name, second_name, "
                "login, password, email) "
                "VALUES "
                "(:group_id, :last_name, :first_name, :second_name, "
                ":login, :password, :email);"
                );
    query.bindValue(":group_id",
                    m_pcbbGroupID->currentData().toString().isEmpty() ?
                        QVariant(QVariant::String) :
                    m_pcbbGroupID->currentData().toString());
    query.bindValue(":last_name", m_pedtLName->text());
    query.bindValue(":first_name", m_pedtFName->text());
    query.bindValue(":second_name", m_pedtSName->text());
    query.bindValue(":login", m_pedtLogin->text());
    query.bindValue(":password", password);
    query.bindValue(":email", m_pedtEmail->text());

    // Запрос на добавление пользователя
    if (!query.exec()) {
        QMessageBox::critical(this,
                              "Ошибка добавления",
                              "Не удалось добавить пользователя",
                              QMessageBox::Ok
                              );
        return;
    }

    QDialog::accept();
}
