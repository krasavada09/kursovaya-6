#ifndef EMAILVALIDATOR_H
#define EMAILVALIDATOR_H

#include "QRegExpValidator"

// Класс валидатора Email

class EmailValidator : public QRegExpValidator {
public:
    EmailValidator(QObject *parent = nullptr);
};

#endif // EMAILVALIDATOR_H
