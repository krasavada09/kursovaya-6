# Курсовая работа - Разработка АРМ сотрудников кредитного отдела

## Файлы проекта

* Описание проекта.docx - Документация проекта
* Database/create-db.sql - Структура базы данных
* Crediting - Исходные файлы C++
* build_MinGW_64_bit-Release/Crediting.exe - Исполняемый файл программы
---

## Запуск программы
### Запуск базы данных
~~~
docker-compose up
~~~
### Запуск клиента
build_MinGW_64_bit-Release/Crediting.exe

### Данные для аутентификации:
* Логин: login
* Пароль: password
---
### Остановка базы данных
~~~
docker-compose down
~~~