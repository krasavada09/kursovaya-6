CREATE TYPE sex AS ENUM ('male', 'female');

CREATE TABLE Groups (
    id serial PRIMARY KEY,
    name varchar(30) NOT NULL,
    r boolean NOT NULL DEFAULT false,
    w boolean NOT NULL DEFAULT false,
    x boolean NOT NULL DEFAULT false
);

CREATE TABLE Users (
    id serial PRIMARY KEY,
    group_id int,
    last_name varchar(30) NOT NULL,
    first_name varchar(30) NOT NULL,
    second_name varchar(30),
    login varchar(20) UNIQUE NOT NULL,
    password varchar(64) NOT NULL,
    email varchar(30),

    FOREIGN KEY (group_id)
    	REFERENCES Groups (id)
    	ON DELETE CASCADE
);

CREATE TABLE Provisions (
    id serial PRIMARY KEY,
    name varchar(50) NOT NULL
);

CREATE TABLE Purposes (
    id serial PRIMARY KEY,
    name varchar(50) NOT NULL
);

CREATE TABLE Repayment_Methods (
    id serial PRIMARY KEY,
    name varchar(50) NOT NULL
);

CREATE TABLE Forms (
    id serial PRIMARY KEY,
    name varchar(50) NOT NULL
);

CREATE TABLE Credit_Ratings (
    id serial PRIMARY KEY,
    name varchar(30) NOT NULL
);

CREATE TABLE Customers (
    id serial PRIMARY KEY,
    passport varchar(11) NOT NULL UNIQUE,
    last_name varchar(30) NOT NULL,
    first_name varchar(30) NOT NULL,
    second_name varchar(30),
    sex sex NOT NULL,
    birth_date date NOT NULL,
    phone varchar(16) NOT NULL,
    address text NOT NULL,
    email varchar(30),
    credit_rating_id int,

    FOREIGN KEY (credit_rating_id)
    	REFERENCES Credit_Ratings (id)
    	ON DELETE RESTRICT
);

CREATE TABLE Credit_Contracts (
    id serial PRIMARY KEY,
    user_id int NOT NULL,
    provision_id int NOT NULL,
    purpose_id int NOT NULL,
    repayment_method_id int NOT NULL,
    form_id int NOT NULL,
    customer_id int NOT NULL,
    amount numeric NOT NULL,
    rate numeric(3,1) NOT NULL,
    term int NOT NULL,
    date date NOT NULL DEFAULT CURRENT_DATE,
    insurance_contract_id int,

    FOREIGN KEY (user_id)
    	REFERENCES Users (id)
    	ON DELETE RESTRICT,
	FOREIGN KEY (provision_id)
		REFERENCES Provisions (id)
		ON DELETE RESTRICT,
	FOREIGN KEY (purpose_id)
		REFERENCES Purposes (id)
		ON DELETE RESTRICT,
	FOREIGN KEY (repayment_method_id)
		REFERENCES Repayment_Methods (id)
		ON DELETE RESTRICT,
	FOREIGN KEY (form_id)
		REFERENCES Forms (id)
		ON DELETE RESTRICT,
	FOREIGN KEY (customer_id)
		REFERENCES Customers (id)
		ON DELETE RESTRICT
);

CREATE TABLE Payment_Schedules (
	id serial PRIMARY KEY,
	credit_contract_id int NOT NULL,
	
	FOREIGN KEY (credit_contract_id)
		REFERENCES Credit_Contracts (id)
		ON DELETE CASCADE
);

CREATE TABLE Schedule_Items (
	id serial PRIMARY KEY,
	payment_schedule_id int NOT NULL,
	date date NOT NULL,
	amount numeric NOT NULL,
	
	FOREIGN KEY (payment_schedule_id)
		REFERENCES Payment_Schedules (id)
		ON DELETE CASCADE
);

CREATE TABLE Payments (
	id serial PRIMARY KEY,
	credit_contract_id int NOT NULL,
	date timestamp NOT NULL DEFAULT current_timestamp,
	amount numeric NOT NULL,
	
	FOREIGN KEY (credit_contract_id)
		REFERENCES Credit_Contracts (id)
		ON DELETE CASCADE
);

INSERT INTO Groups
	(name, r, w, x)
VALUES
	('root', true, true, true),
	('admin', true, true, false),
	('user', true, false, false);

INSERT INTO Credit_Ratings
	(name)
VALUES
	('A+'), ('A'), ('A-'),
	('B+'), ('B'), ('B-'),
	('C+'), ('C'), ('C-'), ('D');

INSERT INTO Provisions
	(name)
VALUES
	('необеспеченный (бланковый)'),
	('залоговый'),
	('гарантированный'),
	('застрахованный');

INSERT INTO Purposes
	(name)
VALUES
	('нецелевой'),
	('образование'),
	('лечение'),
	('строительство или приобретение жилья'),
	('автокредит'),
	('ипотечные ссуды'),
	('приобретение товаров');

INSERT INTO Repayment_Methods
	(name)
VALUES
	('аннуитетный платёж');

INSERT INTO Forms
	(name)
VALUES
	('безналичная'),
	('налично-денежная');

INSERT INTO Users
	(group_id, last_name, first_name, second_name, login, password)
VALUES
	(3, 'Петров', 'Петр', 'Петрович', 'login', '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8');

INSERT INTO Customers
	(passport, last_name, first_name, second_name, sex, birth_date, phone, address, email)
VALUES
	('1111-111111', 'Семенов', 'Сергей', 'Михайлович', 'male', '1986-10-25', '+7(111)111-11-11', 'Мира 5, 23', 'semenov@mail.com'),
	('2222-222222', 'Сергеев', 'Иван', 'Петрович', 'male', '1996-08-12', '+7(222)222-22-22', 'Ленина 3, 12', 'sergeev@mail.com'),
	('3333-333333', 'Михайлов', 'Евгений', 'Семенович', 'male', '1968-12-01', '+7(333)333-33-33', 'Чапаева 18, 2', 'michailov@mail.com');

INSERT INTO Credit_Contracts
	(user_id, provision_id, purpose_id, repayment_method_id, form_id, customer_id, amount, rate, term, date)
VALUES
	(1, 1, 1, 1, 1, 1, 100000, 13.5, 24, current_date),
	(1, 2, 2, 1, 2, 2, 300000, 10.7, 36, current_date),
	(1, 3, 3, 1, 1, 3, 50000, 15.2, 12, current_date);

INSERT INTO Payment_Schedules
	(credit_contract_id)
VALUES
	(1), (2), (1);

INSERT INTO Schedule_Items
	(payment_schedule_id, date, amount)
VALUES
	(1, current_date, 10000),
	(1, current_date + interval '1 month', 10000),
	(1, current_date + interval '2 month', 10000),
	(1, current_date + interval '3 month', 10000),
	(1, current_date + interval '4 month', 10000),
	(1, current_date + interval '5 month', 10000),
	(2, current_date, 25000),
	(2, current_date + interval '1 month', 25000),
	(2, current_date + interval '2 month', 25000),
	(2, current_date + interval '3 month', 25000),
	(2, current_date + interval '4 month', 25000),
	(2, current_date + interval '5 month', 25000),
	(3, current_date, 9500),
	(3, current_date + interval '1 month', 9500),
	(3, current_date + interval '2 month', 9500),
	(3, current_date + interval '3 month', 9500),
	(3, current_date + interval '4 month', 9500),
	(3, current_date + interval '5 month', 9500);

INSERT INTO Payments
	(credit_contract_id, date, amount)
VALUES
	(1, current_date, 4777.7),
	(2, current_date, 9779.05),
	(3, current_date, 4517.64);